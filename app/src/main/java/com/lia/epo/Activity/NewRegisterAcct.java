package com.lia.epo.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class NewRegisterAcct extends AppCompatActivity  implements View.OnClickListener {

     ImageView back_signin;
    Button buttonsignup;
    EditText mailid_signup,password_signup,confpassword_signup;
    public String mailStr,pswdStr;


    private Session session;
    private String URLstringsignup="https://www.vdcprojects.xyz/epo/rest/V1/customers";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_register_acct);
        session = new Session(this);
        if (!session.isNetwork() == true){
            session.nonetwork();
        }
        Log.d("cfvgbhnjm",""+session.isNetwork());
        if (!session.getToken().equals("") && session.isNetwork() == true){
            Intent intent=new Intent(NewRegisterAcct.this, NewSigninActivity.class);
            startActivity(intent);
        }

        back_signin = (ImageView) findViewById(R.id.backto_signin);
        back_signin.setOnClickListener(this);

        password_signup = (EditText)findViewById(R.id.edit_password_signup);
        mailid_signup = (EditText)findViewById(R.id.edit_mailid_signup);
        confpassword_signup= (EditText)findViewById(R.id.edit_confpassword_signup);
        buttonsignup = (Button)findViewById(R.id.button_signup);
        buttonsignup.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        if (view == back_signin) {
            Intent signin=new Intent(NewRegisterAcct.this, NewSigninActivity.class);
            startActivity(signin);
        }

         if(view == buttonsignup){

             InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
             imm.hideSoftInputFromWindow(mailid_signup.getWindowToken(), 0);
            String specialcharreg = ".*[@#!$%^&+=].*";
            String uppercasereg = ".*[A-Z].*";
            String numberreg = ".*[0-9].*";
            String emailid = ".*[@].*";
//            String unameStr = username_signup.getText().toString();
            String pswdStr = password_signup.getText().toString();
            String mailStr = mailid_signup.getText().toString();
            String confpswdStr = confpassword_signup.getText().toString();
            if(mailStr.length()==0 || pswdStr.length()==0 || confpswdStr.length()==0){
//                username_signup.setError("Enter Username");
//                password_signup.setError("Enter Password");
//                mailid_signup.setError("Enter Mailid");
//                confpassword_signup.setError("Enter Password");
//                Snackbar.make(view,"Please Enter All Fields",Snackbar.LENGTH_SHORT).show();
                Toast toast=Toast.makeText(getApplicationContext(),"Please Enter All Fields",Toast.LENGTH_SHORT);
                toast.setMargin(30,30);
                toast.show();
            }
            else if(!mailStr.matches(emailid)){
//                mailid_signup.setError("Enter Valid Mailid");
//                Snackbar.make(view,"Please Enter Valid Mailid",Snackbar.LENGTH_SHORT).show();
                Toast toast=Toast.makeText(getApplicationContext(),"Please Enter Valid Mailid",Toast.LENGTH_SHORT);
                toast.setMargin(30,30);
                toast.show();
            }
            else if(!pswdStr.equals(confpswdStr)){
//                password_signup.setError("Enter Password Correctly");
//                confpassword_signup.setError("Enter Password Correctly");
//                Snackbar.make(view,"Password & Confirm Password Mismatch",Snackbar.LENGTH_SHORT).show();
                Toast toast=Toast.makeText(getApplicationContext(),"Password & Re-Enter Password Mismatch",Toast.LENGTH_SHORT);
                toast.setMargin(30,30);
                toast.show();
            }
            else if((pswdStr.length()<8) || (!pswdStr.matches(specialcharreg)) || (!pswdStr.matches(uppercasereg)) || (!pswdStr.matches(numberreg))){
//                password_signup.setError("Password must have 8 Characters,One UpperCase,Numeric,Special Symbol");
//                confpassword_signup.setError("Password must have 8 Characters,One UpperCase,Numeric,Special Symbol");
//
                Toast toast=Toast.makeText(getApplicationContext(),"Password must have 8 Characters,One UpperCase,Numeric,Special Symbol",Toast.LENGTH_SHORT);
                toast.setMargin(30,30);
                toast.show();
//                Snackbar.make(view,"Password must have 8 Characters,One UpperCase,Numeric,Special Symbol",Snackbar.LENGTH_SHORT).show();
            }

            else{
//                StaticInfo.dialogstart(this,session.getlang());
//                Log.d("mail", ">>" +mailStr);
//                Log.d("mailget", ">>" +session.getmailid());
//                JSONObject requestbody = new JSONObject();
//                try {
//                    requestbody.put("firstname","Abc");
//                    requestbody.put("lastname", "Xyz");
//                    requestbody.put("storeId", "1");
//                    requestbody.put("email", mailStr);
//                    JSONObject mainreqbdy = new JSONObject();
//                    mainreqbdy.put("customer", requestbody);
//                    mainreqbdy.put("password", pswdStr);
//                    signuprequestJSON(mainreqbdy);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
                Snackbar snackbar = Snackbar.make(view,"Signup Success",Snackbar.LENGTH_INDEFINITE);
                Intent n1=new Intent(NewRegisterAcct.this, NewCompleteProfile.class);
                n1.putExtra("mailStr",mailStr);
                n1.putExtra("pswdStr",pswdStr);
                startActivity(n1);
            }
        }Log.d("signup","3333333333333333333333333333333333333");
    }
//    private void signuprequestJSON(JSONObject response) {
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        final String requestBody = response.toString();
//        Log.d("request1",String.valueOf(response));
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLstringsignup, new Response.Listener<String>() {
//
//
//            @Override
//            public void onResponse(String response) {
//                Log.d("signupresponse", ">>" + response);
//                try{
//                    JSONObject obj = new JSONObject(response);
//                    int id = obj.getInt("id");
//                    Log.d("id--", ">>" +id);
//                    session.setUserId(id);
//                    View view = buttonsignup;
//                    Snackbar snackbar = Snackbar.make(view,"Signup Success",Snackbar.LENGTH_INDEFINITE);
//                    Intent n1=new Intent(NewRegisterAcct.this, NewCompleteProfile.class);
//                    n1.putExtra("mailStr",mailStr);
//                    n1.putExtra("pswdStr",pswdStr);
//                    startActivity(n1);
//
//                }
//                catch (JSONException e){
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                StaticInfo.dialogend();
//                Log.e("VOLLEY", error.toString());
//                Toast.makeText(getApplicationContext(),"Mailid already exit",Toast.LENGTH_SHORT).show();
//            }
//        }){
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type","application/json" );
//                Log.d("param",""+params);
//                return params;
//            }
//
//            @Override
//            public String getBodyContentType() {
//                return "application/json; charset=utf-8";
//            }
//            @Override
//            public byte[] getBody() throws AuthFailureError {
//                try {
//                    return requestBody == null ? null : requestBody.getBytes("utf-8");
//                } catch (UnsupportedEncodingException uee) {
//                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
//                    return null;
//                }
//            }
//        };
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        Log.e("VOLLEY1",""+stringRequest);
//        requestQueue.add(stringRequest);
//    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(NewRegisterAcct.this, NewSigninActivity.class);
        startActivity(intent);

    }

  }