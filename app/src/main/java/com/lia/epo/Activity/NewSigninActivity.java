package com.lia.epo.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.TwoStatePreference;
import android.text.method.HideReturnsTransformationMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.prefs.AbstractPreferences;

public class NewSigninActivity extends AppCompatActivity implements View.OnClickListener {
    EditText password_signin,
            username_signin;
    TextView forgot_password, signup;
    Button buttonsignin;
    ImageView google, apple,back_lang;
    CheckBox remember_me;
    private String URLstringsignin = "https://www.vdcprojects.xyz/epo/index.php/rest/V1/integration/customer/token";
    private Session session;
    private boolean isRememberMe;
    private SharedPreferences prefs;
    private String username, password;
    private SharedPreferences.Editor editor;
    private boolean RememberMe;
    public String easyPuzzle;
    private Toast backToast;
    private long backPressedTime;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.epo_login);


        session = new Session(this);
        if (!session.isNetwork() == true) {
            session.nonetwork();
        }

        if (!session.getToken().equals("") && session.isNetwork() == true) {
            Intent intent = new Intent(NewSigninActivity.this, MainActivity.class);
            startActivity(intent);
        }

//        rememberLogin = prefs.getBoolean("rememberLogin", false);
//        if (rememberLogin == true) {
//            username_signin.setText(prefs.getString("mailid", ""));
//            password_signin.setText(prefs.getString("password", ""));
//            remember_me.setChecked(true);
//        }


//        HOOKS
        username_signin = (EditText) findViewById(R.id.edit_mailid_signup);
        password_signin = (EditText) findViewById(R.id.edit_password_signup);
        if (username_signin.equals("")&&password_signin.equals("")){
            username_signin.setText(null);
            password_signin.setText(null);
        }
        username_signin.setText(session.getusername());
        password_signin.setText(session.getpassword());

        forgot_password = (TextView) findViewById(R.id.forgot_password);


        signup = (TextView) findViewById(R.id.button_signup);
        signup.setOnClickListener(this);
        back_lang = (ImageView) findViewById(R.id.back_to_lang);
        back_lang.setOnClickListener(this);

        buttonsignin = (Button) findViewById(R.id.button_signin);
        buttonsignin.setOnClickListener(this);

//        google = (ImageView) findViewById(R.id.google_btn);
//        google.setOnClickListener(this);
//
//        apple = (ImageView) findViewById(R.id.apple_btn);
//        apple.setOnClickListener(this);

        forgot_password = (TextView) findViewById(R.id.forgot_password);
        forgot_password.setOnClickListener(this);
        remember_me = (CheckBox) findViewById(R.id.remember);
//        if (isRememberMe== true) {
//            username_signin.setText(prefs.getString("username", ""));
//            password_signin.setText(prefs.getString("password", ""));
//            remember_me.setChecked(true);
//        }


    }


    @Override
    public void onClick(View view) {


        if (view == forgot_password) {
            Intent forgot = new Intent(NewSigninActivity.this, NewForgotPassword.class);
            startActivity(forgot);
            Log.d("forgot", "111111111111");
        }
        if (view == signup) {
            Intent forgot = new Intent(NewSigninActivity.this, NewRegisterAcct.class);
            startActivity(forgot);
            Log.d("forgot", "222222222");
        }
        if (view==back_lang){
            Intent forgot = new Intent(NewSigninActivity.this, NewLanguage.class);
            startActivity(forgot);
        }
        if (view == buttonsignin) {

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(username_signin.getWindowToken(), 0);

            username = username_signin.getText().toString();
            password = password_signin.getText().toString();

            if (remember_me.isChecked()) {

                session.setusername(username);
                session.setpassword(password);
                Log.d("session22222222", "------->" +
                        session.getpassword());
            } else {
                session.setusername("");
                session.setpassword("");
                Log.d("session3333333", "------->" +
                        session.getpassword());
            }

            String emailid = ".*[@].*";
            String cnameStr = String.valueOf(username_signin.getText().toString());
            String cpswdStr = String.valueOf(password_signin.getText().toString());
            if (username_signin.getText().toString().trim().length() == 0 || password_signin.getText().toString().trim().length() == 0) {
//                username_signin.setError("Enter Username");
//                super.setError("Enter Username", (R.drawable.ic_error_1);
//
//                public void setError(CharSequence error, Drawable icon) {
//
//                    lastErrorIcon = icon;
//                    // if the error is not null, and we are in JB, force
//                    // the error to show
//                    if (error != null /* !isFocused() && */) {
//                        showErrorIconHax(icon);
//                    }
//                }
//                password_signin.setError("Enter Password");
                Toast toast=Toast.makeText(getApplicationContext(),"Please Enter Username and Password",Toast.LENGTH_SHORT);
                toast.setMargin(30,30);
                toast.show();
//                Snackbar.make(view, "Please Enter Username and Password", Snackbar.LENGTH_SHORT).show();

            }

            if (!cnameStr.matches(emailid)) {
//                username_signin.setError("Enter Valid Mailid");
                Toast toast=Toast.makeText(getApplicationContext(),"Please Enter Username and Password",Toast.LENGTH_SHORT);
                toast.setMargin(30,30);
                toast.show();
//                Snackbar.make(view, "Please Enter Valid Mailid", Snackbar.LENGTH_SHORT).show();
            } else if ((cpswdStr.length() < 8)) {
//                password_signin.setError("Enter Password Correctly");
                Toast toast=Toast.makeText(getApplicationContext(),"Please Enter Username and Password",Toast.LENGTH_SHORT);
                toast.setMargin(30,30);
                toast.show();
//                Snackbar.make(view, "Password Length is incorrect", Snackbar.LENGTH_SHORT).show();
            } else {
                StaticInfo.dialogstart(this, session.getlang());
                JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("username", cnameStr);
                    requestbody.put("password", cpswdStr);

                    signinrequestJSON(requestbody);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        }


    }


    private void signinrequestJSON(JSONObject response) {
        {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            final String requestBody = response.toString();
            Log.d("requestsignin", String.valueOf(response));
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URLstringsignin, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    String finalresponse = response.replace('"', ' ');
                    String res = finalresponse.replaceAll(" ", "");
                    Log.d("token", ">>" + finalresponse);
                    Log.d("token", ">>" + res);
                    if (response.toString().trim().length() != 0) {
                        String token = response;
                        //Toast.makeText(getApplicationContext(),token,Toast.LENGTH_SHORT).show();
                        session.setToken(res);
                        View view = buttonsignin;
                        Snackbar.make(view, "SignIn Success", Snackbar.LENGTH_LONG).show();
                        Log.d("eeeeeeeeeeee", "222222222222222222222222");
                        Intent n1 = new Intent(NewSigninActivity.this, MainActivity.class);
                        startActivity(n1);
                    } else {
                        Toast.makeText(getApplicationContext(), "Invalid Details please register first", Toast.LENGTH_SHORT).show();
                        View view = findViewById(R.id.button_signin);
                        Snackbar snackbar = Snackbar.make(view, "Invalid Details please register", Snackbar.LENGTH_SHORT);
                        snackbar.setActionTextColor(Color.RED);
                        View sbview = snackbar.getView();
                        TextView textView = (TextView) sbview.findViewById(R.id.txt_signup);
                        textView.setTextColor(Color.YELLOW);
                        sbview.setBackgroundColor(Color.GRAY);
                        snackbar.show();
                    }
                    StaticInfo.dialogend();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    StaticInfo.dialogend();
                    Log.e("VOLLEY", error.toString());
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }
            };
        /*{
            //This is for Headers If You Needed
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String cnameStr= String.valueOf(username_signin.getText().toString());
                String cpswdStr= String.valueOf(password_signin.getText().toString());
                params.put("username", cnameStr);
                params.put("password", cpswdStr);
                Log.e("VOLLEY2",""+params);
                return params;
            };
            //Pass Your Parameters here
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                String cnameStr= String.valueOf(username_signin.getText().toString());
                String cpswdStr= String.valueOf(password_signin.getText().toString());
                params.put("username", cnameStr);
                params.put("password", cpswdStr);
                Log.e("VOLLEY5",""+params);
                return params;
            }
        };*/
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            Log.e("VOLLEY1", "" + stringRequest);
            requestQueue.add(stringRequest);
        }
    }

    private void setAppLocale(String localeCode) {
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        } else {
            conf.locale = new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
        editor.putString("My_Lang", localeCode);
        editor.apply();
        res.updateConfiguration(conf, dm);
    }

    @Override
    public void onBackPressed() {
//        Intent t = getIntent();
//        String easyPuzzle = t.getExtras().getString("hii");



//        if (easyPuzzle.equals("")) {
            Intent forgot = new Intent(NewSigninActivity.this, NewLanguage.class);
            startActivity(forgot);


//        if (easyPuzzle.equals("hii")) {
//            if (backPressedTime+2000>System.currentTimeMillis()) {
//                backToast.cancel();
//
//                super.onBackPressed();
//                return;
//            }
//            else {
//                backToast=Toast.makeText(getBaseContext(),"Press Back Again to Exit",Toast.LENGTH_SHORT);
//                backToast.show();
//            }
//
//            backPressedTime=System.currentTimeMillis();
//
//        }

    }



    }


