package com.lia.epo.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class NewCompleteProfile extends AppCompatActivity implements View.OnClickListener {
    ImageView back_signin;
    Button buttonsignup;
    EditText name_signup, phone_signup, address_signup;
    private Session session;
    private String userfullname, userphone;

    private String URLstringsignup = "https://www.vdcprojects.xyz/epo/rest/V1/customers";
    private String mailStr, pswdStr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_complete_profile);
        Bundle bundle = getIntent().getExtras();
        mailStr = bundle.getString("mailid");
        pswdStr = bundle.getString("pswdStr");
        session = new Session(this);
        if (!session.isNetwork() == true) {
            session.nonetwork();
        }

        back_signin = (ImageView) findViewById(R.id.backto_register_acct);
        back_signin.setOnClickListener(this);

        name_signup = (EditText) findViewById(R.id.fullname);
        phone_signup = (EditText) findViewById(R.id.phone);
        address_signup = (EditText) findViewById(R.id.address);
        buttonsignup = (Button) findViewById(R.id.epo_signup_continue_btn);
        buttonsignup.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == buttonsignup) {

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(name_signup.getWindowToken(), 0);

            userfullname = name_signup.getText().toString();
            userphone = phone_signup.getText().toString();


            String phoneno = ".*[1234567890+-].*";
            String cnameStr = String.valueOf(name_signup.getText().toString());
            String phone = String.valueOf(phone_signup.getText().toString());
            if (name_signup.getText().toString().trim().length() == 0) {
//                name_signup.setError("Enter Full Name");
                Toast toast=Toast.makeText(getApplicationContext(),"Please Enter All Fields",Toast.LENGTH_SHORT);
                toast.setMargin(30,30);
                toast.show();

//                Snackbar.make(view, "Please Enter Full Name ", Snackbar.LENGTH_SHORT).show();

            }
            if (phone_signup.getText().toString().trim().length() == 0) {
                Toast toast=Toast.makeText(getApplicationContext(),"Please Enter Full Name",Toast.LENGTH_SHORT);
                toast.setMargin(30,30);
                toast.show();

            }
            if (address_signup.getText().toString().trim().length() == 0) {
//                name_signup.setError("Enter Address");
//
//                Snackbar.make(view, "Please Enter Address", Snackbar.LENGTH_SHORT).show();
                Toast toast=Toast.makeText(getApplicationContext(),"Please Enter Address",Toast.LENGTH_SHORT);
                toast.setMargin(30,30);
                toast.show();
            }
            if (name_signup.getText().toString().trim().length() == 0 && phone_signup.getText().toString().trim().length() == 0 && address_signup.getText().toString().trim().length() == 0) {
//                name_signup.setError("Enter Full Name");
//                phone_signup.setError("Enter Phone Number");
//                address_signup.setError("Enter Address");
//                Snackbar.make(view, "Please Enter All fields", Snackbar.LENGTH_SHORT).show();
                Toast toast=Toast.makeText(getApplicationContext(),"Please Enter All Fields",Toast.LENGTH_SHORT);
                toast.setMargin(30,30);
                toast.show();

            }

            if (!phone.matches(phoneno)) {
//                phone_signup.setError("Enter Valid PhoneNumber");
//                Snackbar.make(view, "Please Enter Valid Mailid", Snackbar.LENGTH_SHORT).show();
                Toast toast=Toast.makeText(getApplicationContext(),"Please Enter Valid Mailid",Toast.LENGTH_SHORT);
                toast.setMargin(30,30);
                toast.show();
            } else if ((phone.length() < 10)) {
//                phone_signup.setError("Enter Phone Number Correctly");
                Toast toast=Toast.makeText(getApplicationContext(),"Enter Phone Number Correctly",Toast.LENGTH_SHORT);
                toast.setMargin(30,30);
                toast.show();

            } else {
                StaticInfo.dialogstart(this, session.getlang());
                Log.d("mail", ">>" + mailStr);
                Log.d("mailget", ">>" + session.getmailid());
                JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("firstname", cnameStr);
                    requestbody.put("lastname", "Xyz");
                    requestbody.put("storeId", "1");
                    requestbody.put("email", mailStr);
                    JSONObject mainreqbdy = new JSONObject();
                    mainreqbdy.put("customer", requestbody);
                    mainreqbdy.put("password", pswdStr);
                    signuprequestJSON(mainreqbdy);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        Log.d("signup", "3333333333333333333333333333333333333");
    }

    private void signuprequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request1", String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLstringsignup, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("signupresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    int id = obj.getInt("id");
                    Log.d("id--", ">>" + id);
                    session.setUserId(id);
                    View view = buttonsignup;
                    Snackbar snackbar = Snackbar.make(view, "Signup Success", Snackbar.LENGTH_INDEFINITE);
                    Intent n1 = new Intent(NewCompleteProfile.this, NewSigninActivity.class);
                    startActivity(n1);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
                Toast.makeText(getApplicationContext(), "Mailid already exit", Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                Log.d("param", "" + params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        Intent y = new Intent(NewCompleteProfile.this, NewSigninActivity.class);
        startActivity(y);
    }
}
