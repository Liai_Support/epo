package com.lia.epo.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class AccountActivity extends AppCompatActivity implements View.OnClickListener {
    Button change1,change2;
    ImageView edit_icon,edit_icon1;
    TextView txt_wishlist,txt_cart,txt_orders,settings;

    private Session session;
    LinearLayout mid1,mid2;
    static boolean active = true;
    TextView uname,umail,uname1,uphone,umail1,uaddr,cartcount;
    EditText uname1_edt,uphone_edt,umail1_edt,uaddr_edt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        session = new Session(this);
        uname = (TextView) findViewById(R.id.account_name);
        umail = (TextView) findViewById(R.id.account_mailid);
        uname1 = (TextView) findViewById(R.id.account_name1);
        uphone = (TextView) findViewById(R.id.account_ph_no);
        umail1 = (TextView) findViewById(R.id.account_mailid1);
        uaddr = (TextView) findViewById(R.id.account_addr);

        uname1_edt =(EditText) findViewById(R.id.edit_account_name);
        uphone_edt =(EditText) findViewById(R.id.edit_account_ph_no);
        umail1_edt =(EditText) findViewById(R.id.edit_account_mailid);
        uaddr_edt =(EditText) findViewById(R.id.edit_account_addr);

        txt_wishlist = (TextView)findViewById(R.id.txt_wishlist);
        cartcount = (TextView)findViewById(R.id.txt_cartcount);
        txt_orders = (TextView)findViewById(R.id.txt_orders);
        settings = (TextView)findViewById(R.id.settings);
        txt_wishlist.setOnClickListener(this);
        cartcount.setOnClickListener(this);
        txt_orders.setOnClickListener(this);
        settings.setOnClickListener(this);
        edit_icon = (ImageView)findViewById(R.id.edit_icon);
        edit_icon1 = (ImageView)findViewById(R.id.edit_icon1);
        edit_icon.setOnClickListener(this);
        edit_icon1.setOnClickListener(this);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.bot_nav_account);
        bottomNavigationView.setOnNavigationItemSelectedListener(OnNavigationItemSelectedListener);
        change1 = (Button) findViewById(R.id.change1);
        change2 = (Button) findViewById(R.id.change2);
        change1.setOnClickListener(this);
        change2.setOnClickListener(this);
        mid1 = (LinearLayout) findViewById(R.id.mid1);
        mid2 = (LinearLayout) findViewById(R.id.mid2);
        mid1.setVisibility(VISIBLE);
        //mid2.setVisibility(View.VISIBLE);
        Log.d("accountresponse", ">>");
        //getdata();
        profiledata();
        cartcount.setText(""+session.getcartcount());
    }

    private void profiledata() {
        uname.setText(session.getfirstname());
        umail.setText(session.getmailid());
        uname1.setText(session.getfirstname());
        uphone.setText("");
        umail1.setText(session.getmailid());
        uaddr.setText("");
    }

    private void getdata() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Urls.URLstringviewprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("accountresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    uname.setText(obj.getString("firstname"));
                    umail.setText(obj.getString("email"));
                    uname1.setText(obj.getString("firstname"));
                    uphone.setText("");
                    umail1.setText(obj.getString("email"));
                    uaddr.setText("");
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener OnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.bot_nav_home:
                    Intent n1=new Intent(AccountActivity.this, MainActivity.class);
                    startActivity(n1);
                    return true;
                case R.id.bot_nav_allcategory:
                    Intent n2=new Intent(AccountActivity.this, AllcategoryActivity.class);
                    startActivity(n2);
                    return true;
                case R.id.bot_nav_wishlist:
                    Intent n3=new Intent(AccountActivity.this, WishlistActivity.class);
                    startActivity(n3);
                    return true;
                case R.id.bot_nav_orders:
                    Intent n4=new Intent(AccountActivity.this, OrdersActivity.class);
                    startActivity(n4);
                    return true;
                case R.id.bot_nav_account:
                    if(active == false){
                        Intent n5=new Intent(AccountActivity.this, AccountActivity.class);
                        startActivity(n5);
                    }
                    return true;
            }
            return false;
        }
    };
    
    @Override
    public void onClick(View view) {
        if (view == change1) {
            View v1 = findViewById(R.id.mid2);
            v1.setVisibility(VISIBLE);
            View v2 = findViewById(R.id.mid1);
            v2.setVisibility(GONE);
            uname1_edt.setText(session.getfirstname());
            umail1_edt.setText(session.getmailid());
        }
        else if(view == change2){
            String cnameStr= String.valueOf(uname1_edt.getText().toString());
            String cmailStr= String.valueOf(umail1_edt.getText().toString());
            if(uname1_edt.getText().toString().trim().length()==0 || umail1_edt.getText().toString().trim().length()==0){
                uname1_edt.setError("Enter Username");
                umail1_edt.setError("Enter Password");
                Snackbar.make(view,"Please Enter Username and Password",Snackbar.LENGTH_SHORT).show();
            }
           /* else if(!cnameStr.matches(emailid)){
                umail1_edt.setError("Enter Valid Mailid");
                Snackbar.make(view,"Please Enter Valid Mailid",Snackbar.LENGTH_SHORT).show();
            }
            else if((cpswdStr.length()<8)){
                password_signin.setError("Enter Password Correctly");
                Snackbar.make(view,"Password Length is incorrect",Snackbar.LENGTH_SHORT).show();
            }*/
            else {
                StaticInfo.dialogstart(this,session.getlang());
                JSONObject mainbody = new JSONObject();
                JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("email", cmailStr);
                    requestbody.put("firstname", cnameStr);
                    requestbody.put("lastname", session.getlastname());
                    requestbody.put("website_id",session.getwebsiteid());
                    mainbody.put("customer",requestbody);
                    updateprofilerequestJSON(mainbody);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if (view == edit_icon) {
            View v1 = findViewById(R.id.mid2);
            v1.setVisibility(VISIBLE);
            View v2 = findViewById(R.id.mid1);
            v2.setVisibility(GONE);
        }
        else if(view == edit_icon1){
            View v1 = findViewById(R.id.mid1);
            v1.setVisibility(VISIBLE);
            View v2 = findViewById(R.id.mid2);
            v2.setVisibility(GONE);
        }
        else if(view == txt_wishlist){
            Intent n3=new Intent(AccountActivity.this, WishlistActivity.class);
            startActivity(n3);
        }
        else if(view == cartcount){
            Intent n5=new Intent(AccountActivity.this, CartActivity.class);
            startActivity(n5);
        }
        else if(view == txt_orders){
            Intent n4=new Intent(AccountActivity.this, OrdersActivity.class);
            startActivity(n4);
        }
        else if(view == settings){
            Intent n6=new Intent(AccountActivity.this, SettingActivity.class);
            startActivity(n6);
        }
    }

    private void updateprofilerequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request1",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, Urls.URLstringupdateprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("updateprofileresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    session.setmailid(obj.getString("email"));
                    session.setfirstname(obj.getString("firstname"));
                    session.setlastname(obj.getString("lastname"));
                    session.setwebsiteid(obj.getInt("website_id"));
                    profiledata();
                    View v1 = findViewById(R.id.mid1);
                    v1.setVisibility(VISIBLE);
                    View v2 = findViewById(R.id.mid2);
                    v2.setVisibility(GONE);
                    StaticInfo.dialogend();
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }

}