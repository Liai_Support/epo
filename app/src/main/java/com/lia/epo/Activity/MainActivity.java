package com.lia.epo.Activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.lia.epo.Fragment.HomeFragment;
import com.lia.epo.Fragment.NewHomeFragment;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;
import com.lia.epo.Urls.urlList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private View navHeader;
    private ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtMail,ntitle;
    private Session session;
    static boolean active = true;
    public ArrayList<String> cartsku = new ArrayList<>();
    public TextView textCartItemCount;
    public int cartcount;
    String langcode="en";
    public ProgressDialog progressDialog;
//    private String URLstringviewprofile="https://www.vdcprojects.xyz/epo/rest/V1/customers/me";
//    private String URLstringcartlist="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine";
    public String easyPuzzle;
    private Toast backToast;
    private long backPressedTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        session = new Session(this);


        StaticInfo.dialogstart(this,session.getlang());
        Toolbar toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_burger);
        //toolbar.setLogo(R.drawable.ic_burger);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_home);

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.bot_nav_home);
        //bottomNavigationView.getMenu().getItem(1).setCheckable(false);
        bottomNavigationView.setOnNavigationItemSelectedListener(OnNavigationItemSelectedListener);


        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        txtName = (TextView) navHeader.findViewById(R.id.account_name);
        txtMail = (TextView) navHeader.findViewById(R.id.account_mailid);
        imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);
        getviewprofiledata();
        getcartdata();

        FragmentManager fragmentManager = getSupportFragmentManager();
        NewHomeFragment fragment = new NewHomeFragment();
        fragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).commit();
    }

   /* @Override
    public void onStart(){
        super.onStart();
        active = true;
    }
    @Override
    public void onStop(){
        super.onStop();
        active = false;
    }*/

    private BottomNavigationView.OnNavigationItemSelectedListener OnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.bot_nav_home:
                    if(active == false){
                        Intent n1=new Intent(MainActivity.this, MainActivity.class);
                        startActivity(n1);
                    }
                    return true;
                case R.id.bot_nav_allcategory:
                    Intent n2=new Intent(MainActivity.this, AllcategoryActivity.class);
                    startActivity(n2);
                    return true;
                case R.id.bot_nav_wishlist:
                    Intent n3=new Intent(MainActivity.this, WishlistActivity.class);
                    startActivity(n3);
                    return true;
                case R.id.bot_nav_orders:
                    Intent n4=new Intent(MainActivity.this, OrdersActivity.class);
                    startActivity(n4);
                    return true;
                case R.id.bot_nav_account:
                    BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
                    bottomNavigationView.setSelectedItemId(R.id.bot_nav_home);
                    Intent n5=new Intent(MainActivity.this, AccountActivity.class);
                    startActivity(n5);
                    return true;
            }
            return false;
        }
    };

    private void loadNavHeader() {
        // name, website
        txtName.setText(""+session.getfirstname());
        txtMail.setText(""+session.getmailid());
    }

    private void getviewprofiledata() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlList.stringviewprofile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("accountresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    session.setmailid(obj.getString("email"));
                    session.setfirstname(obj.getString("firstname"));
                    session.setlastname(obj.getString("lastname"));
                    session.setwebsiteid(obj.getInt("website_id"));
                    session.setUserId(obj.getInt("id"));
                    loadNavHeader();
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),"some api issue",Toast.LENGTH_SHORT).show();
                Log.e("VOLLEY", error.toString());
                session.setToken("");
                Intent intent = new Intent(MainActivity.this,NewSigninActivity.class);
                startActivity(intent);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void getcartdata() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlList.stringcartlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("cartresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    obj.getInt("items_count");
                    obj.getInt("items_qty");
                    session.setquoteid(obj.getInt("id"));
                    Log.d("jmkuhj",""+obj.getJSONArray("items"));

                    JSONArray item = obj.getJSONArray("items");
                    session.setcartcount(item.length());
                    cartcount = session.getcartcount();
                    textCartItemCount.setText(""+session.getcartcount());
                    Log.d("dcfvgbhn",""+item.length());
                    for(int i=0;i<item.length();i++){
                        JSONObject childobject = item.getJSONObject(i);
                        cartsku.add(childobject.getString("sku"));
                    }
                    Log.d("ghnjmk",""+cartsku);
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.top_app_bar, menu);
        final MenuItem menuItem = menu.findItem(R.id.cart);

        View actionView = menuItem.getActionView();
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });
        //textCartItemCount.setText("");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.cart) {
            Intent n4=new Intent(MainActivity.this, NewCartActivity.class);
            startActivity(n4);
            return true;
        }
        else if(id == R.id.notification){
            Toast.makeText(getApplicationContext(),"Clicked",Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item){
        int id = item.getItemId();
        Fragment fragment = null;
        switch (id){
            case R.id.nav_home:
                if(active == false){
                    fragment = new HomeFragment();
                }
                break;
            case R.id.nav_acccount:
                Intent n1=new Intent(MainActivity.this, AccountActivity.class);
                startActivity(n1);
                break;
            case R.id.nav_allcategory:
                Intent n2=new Intent(MainActivity.this, AllcategoryActivity.class);
                startActivity(n2);
                break;
            case R.id.nav_orders:
                Intent n3=new Intent(MainActivity.this, OrdersActivity.class);
                startActivity(n3);
                break;
            case R.id.nav_wishlist:
                Intent n4=new Intent(MainActivity.this, WishlistActivity.class);
                startActivity(n4);
                break;
            case R.id.nav_cart:
                Intent n5=new Intent(MainActivity.this, NewCartActivity.class);
                startActivity(n5);
                break;
            case R.id.nav_change_language:
                LayoutInflater layoutInflater = LayoutInflater.from(this);
                View promptView = layoutInflater.inflate(R.layout.languagepopup, null);
                final AlertDialog alertD = new AlertDialog.Builder(this).create();
                Button arabic = (Button) promptView.findViewById(R.id.arabic);
                Button english = (Button) promptView.findViewById(R.id.english);
                arabic.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        langcode = "ar";
                        session.setlang("ar");
                        setAppLocale(langcode);
                        Intent n5=new Intent(MainActivity.this, MainActivity.class);
                        startActivity(n5);
                        alertD.dismiss();
                        // btnAdd1 has been clicked

                    }
                });
                english.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        langcode = "en";
                        session.setlang("en");
                        setAppLocale(langcode);
                        Intent n5=new Intent(MainActivity.this, MainActivity.class);
                        startActivity(n5);
                        alertD.dismiss();
                        // btnAdd2 has been clicked

                    }
                });
                alertD.setView(promptView);
                alertD.setCancelable(true);
                alertD.show();
                alertD.getWindow().setGravity(Gravity.CENTER);
                WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                layoutParams.copyFrom(alertD.getWindow().getAttributes());
                layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                // layoutParams.width = 500;
                // layoutParams.height = 500;
                alertD.getWindow().setAttributes(layoutParams);
                break;
            case R.id.nav_settings:
                Intent n6=new Intent(MainActivity.this, SettingActivity.class);
                startActivity(n6);
                break;
            case R.id.nav_help:
                //fragment = new HomeFragment();
                break;
            case R.id.nav_signout:
                session.setToken("");
               easyPuzzle  = "hii";
                Intent n8=new Intent(MainActivity.this, NewSigninActivity.class);
                n8.putExtra("hii", easyPuzzle);
                startActivity(n8);

                break;
            default:
                break;
        }
        if(fragment != null){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frameLayout,fragment);
            ft.commit();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setAppLocale(String localeCode){
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }
        else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
        editor.putString("My_Lang",localeCode);
        editor.apply();
        res.updateConfiguration(conf,dm);
    }

    @Override
    public void onBackPressed() {
//        moveTaskToBack(true);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //super.onBackPressed();
            if (backPressedTime+2000>System.currentTimeMillis()) {
                backToast.cancel();
                super.onBackPressed();
                return;
            }
            else {
                backToast=Toast.makeText(getBaseContext(),"Press Back Again to Exit",Toast.LENGTH_SHORT);
                backToast.show();
            }
            backPressedTime=System.currentTimeMillis();
        }


    }

}