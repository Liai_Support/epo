package com.lia.epo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

//import com.lia.epo.NewLoginActivity;
import com.lia.epo.R;
import com.lia.epo.Session;

public class NewSplash2Activity extends AppCompatActivity implements View.OnClickListener { RelativeLayout relative1,relative2,relative3;
    ImageView next,next1;
    private Session session;
    Button conitinue_shop;
    TextView skip,skip1;
    private Toast backToast;
    private long backPressedTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash2);
        session = new Session(this);
        if (session.getsplash()== 1){
            Intent intent=new Intent(NewSplash2Activity.this, MainActivity.class);
            startActivity(intent);
        }
        relative1 = (RelativeLayout)findViewById(R.id.relative1);
        relative1.setVisibility(View.VISIBLE);

        relative1.setOnTouchListener(new OnSwipeTouchListener(NewSplash2Activity.this) {
            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
                relative1.setVisibility(View.GONE);
                relative3.setVisibility(View.GONE);
                relative2.setVisibility(View.VISIBLE);
            }
            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
            }
        });

        relative2 = (RelativeLayout)findViewById(R.id.relative2);
        relative2.setOnTouchListener(new OnSwipeTouchListener(NewSplash2Activity.this) {
            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
                relative1.setVisibility(View.GONE);
                relative2.setVisibility(View.GONE);
                relative3.setVisibility(View.VISIBLE);
            }
            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                relative1.setVisibility(View.VISIBLE);
                relative2.setVisibility(View.GONE);
                relative3.setVisibility(View.GONE);
            }
        });
        relative3 = (RelativeLayout)findViewById(R.id.relative3);
        relative3.setOnTouchListener(new OnSwipeTouchListener(NewSplash2Activity.this) {
            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
            }
            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                relative1.setVisibility(View.GONE);
                relative2.setVisibility(View.VISIBLE);
                relative3.setVisibility(View.GONE);
            }
        });
        next = (ImageView) findViewById(R.id.next);
        next1 = (ImageView) findViewById(R.id.next1);
        conitinue_shop = (Button) findViewById(R.id.continue_shop);
        skip = (TextView)findViewById(R.id.skip);
        skip1 = (TextView)findViewById(R.id.skip1);
        next.setOnClickListener(this);
        next1.setOnClickListener(this);
        skip.setOnClickListener(this);
        skip1.setOnClickListener(this);
        conitinue_shop.setOnClickListener(this);
    }
    @Override
    public void onClick(View view) {
        if (view == next) {
            int splash = 1;
            session.setsplash(splash);
            relative1.setVisibility(View.GONE);
            relative3.setVisibility(View.GONE);
            relative2.setVisibility(View.VISIBLE);
        }
        else if(view == next1){
            int splash = 1;
            session.setsplash(splash);
            relative1.setVisibility(View.GONE);
            relative2.setVisibility(View.GONE);
            relative3.setVisibility(View.VISIBLE);
        }
        else if(view == skip){
            int splash = 1;
            session.setsplash(splash);
            Intent n1=new Intent(NewSplash2Activity.this, MainActivity.class);
            startActivity(n1);
        }
        else if(view == skip1){
            int splash = 1;
            session.setsplash(splash);
            Intent n1=new Intent(NewSplash2Activity.this, MainActivity.class);
            startActivity(n1);
        }
        else if(view == conitinue_shop){
            int splash = 1;
            session.setsplash(splash);
            Intent n1=new Intent(NewSplash2Activity.this, MainActivity.class);
            startActivity(n1);
        }
    }
    @Override
    public void onBackPressed() {
//        if (backPressedTime+2000>System.currentTimeMillis()) {
//            backToast.cancel();
//
//            super.onBackPressed();
//            return;
//        }
//        else {
//            backToast=Toast.makeText(getBaseContext(),"Press Back Again to Exit",Toast.LENGTH_SHORT);
//            backToast.show();
//        }
//
//        backPressedTime=System.currentTimeMillis();
//     Intent intent=new Intent(this,NewLanguage.class);
////        startActivity(intent);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      finish();

    }

}
