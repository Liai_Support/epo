package com.lia.epo.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.lia.epo.Adapter.OrderSummaryAdapter;
import com.lia.epo.Data.OrderSummaryData;
import com.lia.epo.DataModel.OrderSummaryDataModel;
import com.lia.epo.R;

import java.util.ArrayList;

public class OrderdetailsActivity extends AppCompatActivity implements View.OnClickListener{
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    ImageView back;
    private static ArrayList<OrderSummaryDataModel> data;
    static View.OnClickListener myOnClickListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderdetails);
        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener((View.OnClickListener) this);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.bot_nav_orders);
        bottomNavigationView.setOnNavigationItemSelectedListener(OnNavigationItemSelectedListener);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<OrderSummaryDataModel>();
        for (int i = 0; i < OrderSummaryData.nameArray.length; i++) {
            data.add(new OrderSummaryDataModel(
                    OrderSummaryData.nameArray[i],
                    OrderSummaryData.versionArray[i],
                    OrderSummaryData.id_[i],
                    OrderSummaryData.drawableArray[i]
            ));
        }

        /*adapter = new OrderSummaryAdapter(data);
        recyclerView.setAdapter(adapter);*/
    }
    @Override
    public void onClick(View view) {
        if (view == back) {
            Intent n1=new Intent(OrderdetailsActivity.this, OrdersActivity.class);
            startActivity(n1);
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener OnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.bot_nav_home:
                    Intent n1=new Intent(OrderdetailsActivity.this, MainActivity.class);
                    startActivity(n1);
                    return true;
                case R.id.bot_nav_allcategory:
                    Intent n2=new Intent(OrderdetailsActivity.this, AllcategoryActivity.class);
                    startActivity(n2);
                    return true;
                case R.id.bot_nav_wishlist:
                    Intent n3=new Intent(OrderdetailsActivity.this, WishlistActivity.class);
                    startActivity(n3);
                    return true;
                case R.id.bot_nav_orders:
                    Intent n4=new Intent(OrderdetailsActivity.this, OrdersActivity.class);
                    startActivity(n4);
                    return true;
                case R.id.bot_nav_account:
                    Intent n5=new Intent(OrderdetailsActivity.this, AccountActivity.class);
                    startActivity(n5);
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onBackPressed() {
        Intent y= new Intent(OrderdetailsActivity.this,OrdersActivity.class);
        startActivity(y);
    }
}