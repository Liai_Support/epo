package com.lia.epo.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.lia.epo.Adapter.HomeSuggestionAdapter;
import com.lia.epo.DataModel.ProductDataModel;
import com.lia.epo.DataModel.ProductlistDataModel;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.Adapter.ProductlistAdapter;
import com.lia.epo.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductlistActivity extends AppCompatActivity implements View.OnClickListener{
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private GridLayoutManager gridLayoutManager;
    private static RecyclerView recyclerView;
    private Session session;
    TextView categoryname;
    public ArrayList<String> cartsku = new ArrayList<>();;
    public TextView textCartItemCount;
    public int cartcount,head=80;
    String categeryname;
    ImageView back;
    int categoryid;
    private static ArrayList<ProductlistDataModel> data;
    private String URLstringcartlist="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine";
    private static final String producturl = "https://www.vdcprojects.xyz/epo/index.php/rest/V1/products";
    private static final String Url = "https://www.vdcprojects.xyz/epo/rest/V1/products?searchCriteria[filter_groups][0][filters][0][field]=category_id&searchCriteria[filter_groups][0][filters][0][value]=12";
    private static  final String imgurl = "https://www.vdcprojects.xyz/epo/pub/media/catalog/product";
    static View.OnClickListener myOnClickListener;
    private static final String url1 = "https://www.vdcprojects.xyz/epo/index.php/rest/english/V1/categories/";
    private static final String url2 = "https://www.vdcprojects.xyz/epo/index.php/rest/english/V1/products?searchCriteria[filter_groups][0][filters][0][field]=sku&searchCriteria[filter_groups][0][filters][0][value]=";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productlist);
        session = new Session(this);
        StaticInfo.dialogstart(this,session.getlang());
        data = new ArrayList<>();
        categoryid = getIntent().getIntExtra("categeryid",0);
        categeryname = getIntent().getStringExtra("categoryname");
        //cartsku = getIntent().getStringArrayListExtra("cartlist");
        getcartdata();
        Toolbar toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        Log.d("cartCount",""+session.getcartcount());
        back = (ImageView)findViewById(R.id.back);
        categoryname = (TextView) findViewById(R.id.category_name);
        categoryname.setText(""+categeryname);
        back.setOnClickListener((View.OnClickListener) this);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_UNLABELED);
        bottomNavigationView.getMenu().getItem(0).setCheckable(false);
        bottomNavigationView.setOnNavigationItemSelectedListener(OnNavigationItemSelectedListener);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        gridLayoutManager =new GridLayoutManager(this,2,GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);
        //recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //productbycategory();
    }

    @Override
    public void onClick(View view) {
        if (view == back) {
            onBackPressed();
           /* Intent n1=new Intent(ProductlistActivity.this, AllcategoryActivity.class);
            startActivity(n1);*/
        }
    }


    private void getcartdata() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLstringcartlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("cartresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    obj.getInt("items_count");
                    obj.getInt("items_qty");
                    session.setquoteid(obj.getInt("id"));
                    Log.d("jmkuhj",""+obj.getJSONArray("items"));
                    JSONArray item = obj.getJSONArray("items");
                    session.setcartcount(item.length());
                    textCartItemCount.setText(""+session.getcartcount());
                    Log.d("dcfvgbhn",""+item.length());
                    for(int i=0;i<item.length();i++){
                        JSONObject childobject = item.getJSONObject(i);
                        cartsku.add(childobject.getString("sku"));
                    }
                    Log.d("ghnjmk",""+cartsku);
                    if(categeryname.equals(getString(R.string.allproducts))){
                        product();
                    }
                    else {
                        url1function(categoryid);
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                StaticInfo.dialogend();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void product() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,  producturl+"?searchCriteria[page_size]="+head, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("product", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    JSONArray dataa = obj.getJSONArray("items");
                    Log.d("gvbhjn",""+dataa.length());
                    data = new ArrayList<>();
                    for(int i=0;i < dataa.length();i++) {
                        JSONObject childobject = dataa.getJSONObject(i);
                        childobject.getString("name");
                        JSONArray img = childobject.getJSONArray("media_gallery_entries");
                        JSONObject imgobj = img.getJSONObject(0);
                        imgobj.getString("file");
                        // Log.d("gbhnj",""+imgurl+imgobj.getString("file"));
                        //Log.d("hnjmk,",""+activity.cartsku);
                        String image = imgurl+imgobj.getString("file");
                        ProductlistDataModel productlistDataModel= new ProductlistDataModel(image,childobject.getString("sku"),childobject.getString("name"),childobject.getInt("id"),childobject.getDouble("price"));
                        data.add(productlistDataModel);
                        recyclerView.setAdapter(new ProductlistAdapter(getApplicationContext(),data,ProductlistActivity.this));
                    }
                    StaticInfo.dialogend();
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());

                Log.d("parasfscm",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            public Map<String, String> getParams() throws AuthFailureError{
                Map<String, String> params1 = new HashMap<String, String>();
                params1.put("searchCriteria[page_size]","5");
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                Log.d("cfgvb",""+params1);
                return params1;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                   /* try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }*/
                return null;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }


    private void url1function(int categoryid) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,  url1+categoryid+"/products", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("urlresponse", ">>" + response);
                try{
                    JSONArray arr = new JSONArray(response);
                    for(int i=0;i < arr.length();i++) {
                        JSONObject childobject = arr.getJSONObject(i);
                        String sku = childobject.getString("sku");
                        Log.d("sku",""+sku);
                        url2function(sku);
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                   /* try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }*/
                return null;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void url2function(String sku) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,  url2+sku, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("url2response", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    JSONArray dataa = obj.getJSONArray("items");
                    Log.d("gvbhjnvghbj",""+dataa.length());
                   // data = new ArrayList<>();
                   for(int i=0;i < dataa.length();i++) {
                        JSONObject childobject = dataa.getJSONObject(i);
                        childobject.getString("name");
                        JSONArray img = childobject.getJSONArray("media_gallery_entries");
                        JSONObject imgobj = img.getJSONObject(0);
                        imgobj.getString("file");
                        Log.d("imgurl",""+imgurl+imgobj.getString("file"));
                        String image = imgurl+imgobj.getString("file");
                        ProductlistDataModel productlistDataModel= new ProductlistDataModel(image,childobject.getString("sku"),childobject.getString("name"),childobject.getInt("id"),childobject.getDouble("price"));
                        data.add(productlistDataModel);
                        recyclerView.setAdapter(new ProductlistAdapter(getApplicationContext(),data,ProductlistActivity.this));
                    }
                   StaticInfo.dialogend();
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                   /* try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }*/
                return null;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }



   /* private void productbycategory() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,  Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("productlistbycategory", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    JSONArray dataa = obj.getJSONArray("items");
                    Log.d("gvbhjnvghbj",""+dataa.length());
                    data = new ArrayList<>();
                    for(int i=0;i < dataa.length();i++) {
                        JSONObject childobject = dataa.getJSONObject(i);
                        childobject.getString("name");
                        JSONArray img = childobject.getJSONArray("media_gallery_entries");
                        JSONObject imgobj = img.getJSONObject(0);
                        imgobj.getString("file");
                        Log.d("gbhnj",""+imgurl+imgobj.getString("file"));
                        String image = imgurl+imgobj.getString("file");
                        ProductlistDataModel productlistDataModel= new ProductlistDataModel(image,childobject.getString("sku"),childobject.getString("name"),childobject.getInt("id"),childobject.getDouble("price"));
                        data.add(productlistDataModel);
                        recyclerView.setAdapter(new ProductlistAdapter(getApplicationContext(),data,ProductlistActivity.this));
                    }

                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());

                Log.d("parasfscm",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            public Map<String, String> getParams() throws AuthFailureError{
                Map<String, String> params1 = new HashMap<String, String>();
                params1.put("searchCriteria[page_size]","5");
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                Log.d("cfgvb",""+params1);
                return params1;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                   *//* try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }*//*
                return null;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }*/

    private BottomNavigationView.OnNavigationItemSelectedListener OnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.bot_nav_home:
                    Intent n1=new Intent(ProductlistActivity.this, MainActivity.class);
                    startActivity(n1);
                    return true;
                case R.id.bot_nav_allcategory:
                    Intent n2=new Intent(ProductlistActivity.this, AllcategoryActivity.class);
                    startActivity(n2);
                    return true;
                case R.id.bot_nav_wishlist:
                    Intent n3=new Intent(ProductlistActivity.this, WishlistActivity.class);
                    startActivity(n3);
                    return true;
                case R.id.bot_nav_orders:
                    Intent n4=new Intent(ProductlistActivity.this, OrdersActivity.class);
                    startActivity(n4);
                    return true;
                case R.id.bot_nav_account:
                    Intent n5=new Intent(ProductlistActivity.this, AccountActivity.class);
                    startActivity(n5);
                    return true;
            }
            return false;
        }
    };




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_app_bar_cartn_filter, menu);
        final MenuItem menuItem = menu.findItem(R.id.cart);

        View actionView = menuItem.getActionView();
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);
        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });
        //textCartItemCount.setText("");
        textCartItemCount.setText(""+session.getcartcount());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.cart) {
            Intent n4=new Intent(ProductlistActivity.this, CartActivity.class);
            startActivity(n4);
            return true;
        }
        else if(id == R.id.filter){
            Toast.makeText(getApplicationContext(),"Clicked",Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

}