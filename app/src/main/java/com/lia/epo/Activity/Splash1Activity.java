package com.lia.epo.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.lia.epo.R;
import com.lia.epo.Session;

public class Splash1Activity extends AppCompatActivity implements Animation.AnimationListener {
    ImageView logo;
    Animation blink;
    Animation animFadeIn;
    RelativeLayout relativeLayout;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash1);
        session = new Session(this);
        logo = (ImageView)findViewById(R.id.logo);
        /*blink = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.blink);
        logo.setVisibility(View.VISIBLE);
        logo.startAnimation(blink);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent n=new Intent(Splash1Activity.this, Splash2Activity.class);
                startActivity(n);
            }
        });
*/
        // load the animation
        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.animation_fade_in);
        // set animation listener
        animFadeIn.setAnimationListener(this);
        // animation for image
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);
        // start the animation
        relativeLayout.setVisibility(View.VISIBLE);
        relativeLayout.startAnimation(animFadeIn);
    }

    @Override
    public void onAnimationStart(Animation animation) {
        //under Implementation
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (session.isNetwork() == true){
            Intent n=new Intent(Splash1Activity.this, Splash2Activity.class);
            startActivity(n);
            this.finish();
        }
        else {
            AlertDialog.Builder alertDialog3 = new AlertDialog.Builder(Splash1Activity.this);
            // LayoutInflater inflater = getLayoutInflater();
            LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialoglayout = inflater.inflate(R.layout.no_network, null);
            dialoglayout.setMinimumHeight(100);
            dialoglayout.setMinimumWidth(100);
            dialoglayout.setBackgroundDrawable(null);
            alertDialog3.setView(R.layout.no_network);
            final AlertDialog alertDialog = alertDialog3.create();
            alertDialog.show();
            alertDialog.show();
            alertDialog.setCancelable(true);
            alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    // dialog dismiss without button press
                    if (session.isNetwork() == true){
                        alertDialog.dismiss();
                        Intent i = new Intent(Splash1Activity.this, Splash1Activity.class);
                        startActivity(i);
                    }
                    else {
                        alertDialog.show();
                    }
                }
            });
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
            layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
            layoutParams.width = 500;
            layoutParams.height = 500;
            alertDialog.getWindow().setBackgroundDrawable(null);
            alertDialog.getWindow().setAttributes(layoutParams);
            ImageView btnClose = (ImageView)alertDialog.findViewById(R.id.btnIvClose);
            btnClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    if (session.isNetwork() == true){
                        alertDialog.dismiss();
                        Intent i = new Intent(Splash1Activity.this, Splash1Activity.class);
                        startActivity(i);
                    }
                    else {
                        alertDialog.show();
                    }
                }
            });
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

}