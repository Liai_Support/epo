package com.lia.epo.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lia.epo.R;
import com.lia.epo.Session;

public class SplashNew extends AppCompatActivity  {
    //    VAR
    ImageView logo;
    Animation animFadeIn;
    RelativeLayout relativeLayout;
    private Session session;
    private Window mWindow;
    private static int SPLASHSCREENVISIBLE = 3000; //    3 sec

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash1);


//        HIDING STATUS BAR(manifest,style)
        mWindow = getWindow();
        mWindow.getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

//            LOAD THE ANIMATION
        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.animation_fade_in);

//           HOOKS
        session = new Session(this);
        logo = (ImageView)findViewById(R.id.logo);
        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);

//            SET ANIMATION
        logo.setAnimation(animFadeIn);
        relativeLayout.setAnimation(animFadeIn);


//            DELAY METHOD
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
//                    TO MOVE MAIN ACTIVITY
                if (session.isNetwork() == true) {
                    Intent intent = new Intent(SplashNew.this, NewLanguage.class);
                    startActivity(intent);
                    finish();
                }

                else {
                    AlertDialog.Builder alertDialog3 = new AlertDialog.Builder(SplashNew.this);
                    LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View dialoglayout = inflater.inflate(R.layout.no_network, null);
                    dialoglayout.setMinimumHeight(100);
                    dialoglayout.setMinimumWidth(100);
                    dialoglayout.setBackgroundDrawable(null);
                    alertDialog3.setView(R.layout.no_network);
                    final AlertDialog alertDialog = alertDialog3.create();
                    alertDialog.show();
                    alertDialog.show();
                    alertDialog.setCancelable(true);
                    alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            // dialog dismiss without button press
                            if (session.isNetwork() == true){
                                alertDialog.dismiss();
                                Intent i = new Intent(SplashNew.this, SplashNew.class);
                                startActivity(i);
                            }
                            else {
                                alertDialog.show();
                            }
                        }
                    });
                    WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
                    layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
                    layoutParams.width = 500;
                    layoutParams.height = 500;
                    alertDialog.getWindow().setBackgroundDrawable(null);
                    alertDialog.getWindow().setAttributes(layoutParams);
                    ImageView btnClose = (ImageView)alertDialog.findViewById(R.id.btnIvClose);
                    btnClose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View arg0) {
                            if (session.isNetwork() == true){
                                alertDialog.dismiss();
                                Intent i = new Intent(SplashNew.this, SplashNew.class);
                                startActivity(i);
                            }
                            else {
                                alertDialog.show();
                            }
                        }
                    });
                }
            }

        }, SPLASHSCREENVISIBLE);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}