package com.lia.epo.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.lia.epo.R;

public class FooterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_footer);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_SELECTED);
        //bottomNavigationView.dispatchSetSelected(true);
        //bottomNavigationView.setSelected(false);
        bottomNavigationView.setSelectedItemId(R.id.bot_nav_wishlist);
        bottomNavigationView.getMenu().getItem(1).setCheckable(false);
        bottomNavigationView.setOnNavigationItemSelectedListener(OnNavigationItemSelectedListener);
    }
    private BottomNavigationView.OnNavigationItemSelectedListener OnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.bot_nav_home:
                    Intent n1=new Intent(FooterActivity.this, MainActivity.class);
                    startActivity(n1);
                    return true;
                case R.id.bot_nav_allcategory:
                    Intent n2=new Intent(FooterActivity.this, AllcategoryActivity.class);
                    startActivity(n2);
                    return true;
                case R.id.bot_nav_wishlist:
                    Intent n3=new Intent(FooterActivity.this, WishlistActivity.class);
                    startActivity(n3);
                    return true;
                case R.id.bot_nav_orders:
                    Intent n4=new Intent(FooterActivity.this, OrdersActivity.class);
                    startActivity(n4);
                    return true;
                case R.id.bot_nav_account:
                    Intent n5=new Intent(FooterActivity.this, AccountActivity.class);
                    startActivity(n5);
                    return true;
            }
            return false;
        }
    };
}