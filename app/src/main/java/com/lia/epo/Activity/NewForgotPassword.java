package com.lia.epo.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.lia.epo.R;

public class NewForgotPassword extends AppCompatActivity implements View.OnClickListener {
    ImageView back_signin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_forgot_password);

        back_signin = (ImageView) findViewById(R.id.backto_signin);
        back_signin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == back_signin) {
            Intent signin=new Intent(NewForgotPassword.this, NewSigninActivity.class);
            startActivity(signin);
        }
    }
}