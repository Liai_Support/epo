package com.lia.epo.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.lia.epo.R;
import com.lia.epo.Adapter.WishlistAdapter;
import com.lia.epo.Data.WishlistData;
import com.lia.epo.DataModel.WishlistDataModel;

import java.util.ArrayList;

public class WishlistActivity extends AppCompatActivity implements View.OnClickListener{
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    ImageView back;
    TextView clearall;
    Button addtocart;
    static boolean active = true;
    private static ArrayList<WishlistDataModel> data;
    static View.OnClickListener myOnClickListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wishlist);
        Toolbar toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener((View.OnClickListener) this);
        clearall = (TextView)findViewById(R.id.clearall);
        clearall.setOnClickListener(this);
        addtocart = (Button)findViewById(R.id.addtocart);
        addtocart.setOnClickListener(this);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.bot_nav_wishlist);
        bottomNavigationView.setOnNavigationItemSelectedListener(OnNavigationItemSelectedListener);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<WishlistDataModel>();
        for (int i = 0; i < WishlistData.nameArray.length; i++) {
            data.add(new WishlistDataModel(
                    WishlistData.nameArray[i],
                    WishlistData.versionArray[i],
                    WishlistData.id_[i],
                    WishlistData.drawableArray[i]
            ));
        }

        adapter = new WishlistAdapter(data);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_app_bar_cartn_filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.cart) {
            Intent n4=new Intent(WishlistActivity.this, CartActivity.class);
            startActivity(n4);
            return true;
        }
        else if(id == R.id.filter){
            Toast.makeText(getApplicationContext(),"Clicked",Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view == back) {
            Intent n1=new Intent(WishlistActivity.this, MainActivity.class);
            startActivity(n1);
        }
        else if(view == clearall){

        }
        else if(view == addtocart){
            Intent n1=new Intent(WishlistActivity.this, CartActivity.class);
            startActivity(n1);
        }
    }
    private BottomNavigationView.OnNavigationItemSelectedListener OnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.bot_nav_home:
                    Intent n1=new Intent(WishlistActivity.this, MainActivity.class);
                    startActivity(n1);
                    return true;
                case R.id.bot_nav_allcategory:
                    Intent n2=new Intent(WishlistActivity.this, AllcategoryActivity.class);
                    startActivity(n2);
                    return true;
                case R.id.bot_nav_wishlist:
                    if(active == false){
                        Intent n4=new Intent(WishlistActivity.this, WishlistActivity.class);
                        startActivity(n4);
                    }
                    return true;
                case R.id.bot_nav_orders:
                    Intent n4=new Intent(WishlistActivity.this, OrdersActivity.class);
                    startActivity(n4);
                    return true;
                case R.id.bot_nav_account:
                    Intent n5=new Intent(WishlistActivity.this, AccountActivity.class);
                    startActivity(n5);
                    return true;
            }
            return false;
        }
    };

}