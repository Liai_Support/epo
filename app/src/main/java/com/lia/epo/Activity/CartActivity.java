package com.lia.epo.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.lia.epo.Adapter.CartAdapter;
import com.lia.epo.Adapter.ProductlistAdapter;
import com.lia.epo.DataModel.CartDataModel;
import com.lia.epo.DataModel.ProductlistDataModel;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CartActivity extends AppCompatActivity implements View.OnClickListener{
    private static RecyclerView.Adapter adapter;
    ImageView back;
    static boolean active = true;
    Button procced_checkout;
    public int itemcount;
    public Double totalamount=0.0;
    public TextView clear;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    public String cartimgurl;
    private static ArrayList<CartDataModel> data;
    private String URLstringdeletecart="https://www.vdcprojects.xyz/epo/index.php/rest//V1/carts/mine/items/";
    private String URLstringcartlist="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine";
    private static final String url2 = "https://www.vdcprojects.xyz/epo/index.php/rest/english/V1/products?searchCriteria[filter_groups][0][filters][0][field]=sku&searchCriteria[filter_groups][0][filters][0][value]=";
    private static  final String imgurl = "https://www.vdcprojects.xyz/epo/pub/media/catalog/product";

    static View.OnClickListener myOnClickListener;
    public Session session;
    public TextView orderamount,textCartItemCount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        session = new Session(this);
        StaticInfo.dialogstart(this,session.getlang());
        orderamount = (TextView) findViewById(R.id.order_amount);
        clear = (TextView) findViewById(R.id.clear);
        clear.setOnClickListener(this);
        Toolbar toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener((View.OnClickListener) this);
        procced_checkout =(Button)findViewById(R.id.proceed_checkout);
        procced_checkout.setOnClickListener(this);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_UNLABELED);
        bottomNavigationView.getMenu().getItem(0).setCheckable(false);
        bottomNavigationView.setOnNavigationItemSelectedListener(OnNavigationItemSelectedListener);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        getcartdata();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_app_bar_cartn_filter, menu);
        final MenuItem menuItem = menu.findItem(R.id.cart);

        View actionView = menuItem.getActionView();
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });
        //textCartItemCount.setText("");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.cart) {
            if(active == false){
                Intent n4=new Intent(CartActivity.this, CartActivity.class);
                startActivity(n4);
            }
            return true;
        }
        else if(id == R.id.filter){
            Toast.makeText(getApplicationContext(),"Clicked",Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view == back) {
            Intent n1=new Intent(CartActivity.this, MainActivity.class);
            startActivity(n1);
        }
        else if(view == procced_checkout){
            if(textCartItemCount.getText().toString().equals("0")){
                Toast.makeText(this,"Cart is empty",Toast.LENGTH_SHORT).show();
            }
            else {
                Intent n2=new Intent(CartActivity.this, CheckoutActivity.class);
                n2.putExtra("itemcount",itemcount);
                n2.putExtra("totalamount",totalamount);
                startActivity(n2);
            }
        }
        else if(view == clear){
            if(data.size()>0){
                StaticInfo.dialogstart(this,session.getlang());
                for(int i=0;i<data.size();i++){
                    deletecartdata(data.get(i).getItemid());
                }
                Intent n76 = new Intent(CartActivity.this, CartActivity.class);
                StaticInfo.dialogend();
                startActivity(n76);
            }
            /*session.setcartcount(0);
            textCartItemCount.setText(""+session.getcartcount());*/
        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener OnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.bot_nav_home:
                    Intent n1=new Intent(CartActivity.this, MainActivity.class);
                    startActivity(n1);
                    return true;
                case R.id.bot_nav_allcategory:
                    Intent n2=new Intent(CartActivity.this, AllcategoryActivity.class);
                    startActivity(n2);
                    return true;
                case R.id.bot_nav_wishlist:
                    Intent n3=new Intent(CartActivity.this, WishlistActivity.class);
                    startActivity(n3);
                    return true;
                case R.id.bot_nav_orders:
                    Intent n4=new Intent(CartActivity.this, OrdersActivity.class);
                    startActivity(n4);
                    return true;
                case R.id.bot_nav_account:
                    Intent n5=new Intent(CartActivity.this, AccountActivity.class);
                    startActivity(n5);
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onBackPressed() {

    }

    private void deletecartdata(int itemid) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, URLstringdeletecart+itemid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("deletecartresponse", ">>" + response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }


    private void getcartdata() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLstringcartlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("cartresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    obj.getInt("items_count");
                    obj.getInt("items_qty");
                    JSONArray dataa = new JSONArray(obj.getString("items"));
                    session.setcartcount(dataa.length());
                    textCartItemCount.setText(""+session.getcartcount());
                    data = new ArrayList<>();
                    itemcount = dataa.length();
                    if(dataa.length()>0){
                        for(int i=0;i < dataa.length();i++) {
                            JSONObject childobject = dataa.getJSONObject(i);
                            childobject.getString("name");
                            childobject.getDouble("price");
                            childobject.getDouble("qty");
                            url2function(childobject.getString("sku"),childobject.getString("name"),childobject.getDouble("price"),childobject.getInt("item_id"),childobject.getInt("qty"));
                       /* CartDataModel cartDataModel= new CartDataModel(childobject.getString("sku"),childobject.getString("name"),childobject.getDouble("price"),childobject.getInt("item_id"),childobject.getInt("qty"));
                        data.add(cartDataModel);*/
                            //setupAllcategoryData(data);
                            //  Log.d("teli", "" +childobject);
                            Log.d("teli11", String.valueOf(childobject.getString("name")));
                            Log.d("teli1", String.valueOf(childobject.getInt("item_id")));
                        }
                    }
                    else {
                        StaticInfo.dialogend();
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                StaticInfo.dialogend();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void url2function(final String sku, final String name, final Double price, final int item_id, final int qty) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,  url2+sku, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("url2response", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    JSONArray dataa = obj.getJSONArray("items");
                    Log.d("gvbhjnvghbj",""+dataa.length());
                    // data = new ArrayList<>();
                    JSONObject childobject = dataa.getJSONObject(0);
                    childobject.getString("name");
                    JSONArray img = childobject.getJSONArray("media_gallery_entries");
                    JSONObject imgobj = img.getJSONObject(0);
                    imgobj.getString("file");
                    Log.d("imgurl",""+imgurl+imgobj.getString("file"));
                    String image = imgurl+imgobj.getString("file");
                    Log.d("nfnvhurvh",""+cartimgurl);

                    CartDataModel cartDataModel= new CartDataModel(image,sku,name,price,item_id,qty);
                    data.add(cartDataModel);
                    adapter = new CartAdapter(data,CartActivity.this,getApplicationContext());
                    recyclerView.setAdapter(adapter);
                    StaticInfo.dialogend();
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                StaticInfo.dialogend();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                   /* try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }*/
                return null;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

}