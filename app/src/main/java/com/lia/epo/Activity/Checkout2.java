package com.lia.epo.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.lia.epo.Fragment.CheckoutDeliveryinfoFragment;
import com.lia.epo.Fragment.CheckoutOrderplacedFragment;
import com.lia.epo.Fragment.CheckoutOrdersummuryFragment;
import com.lia.epo.Fragment.CheckoutPaymentFragment;
import com.lia.epo.R;
import com.lia.epo.Session;

import org.json.JSONObject;

public class Checkout2 extends AppCompatActivity implements View.OnClickListener{
    ImageView back;
    public int itemcount;
    public Double subtotal;
    public String paymentcode,shipping_amount;
    public String orderid;
    public JSONObject billingdata;
    public Session session;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout2);
        session = new Session(this);
        itemcount = getIntent().getIntExtra("itemcount",0);
        subtotal = getIntent().getDoubleExtra("totalamount",0.0);
        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener((View.OnClickListener) this);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_UNLABELED);
        bottomNavigationView.getMenu().getItem(0).setCheckable(false);
        bottomNavigationView.setOnNavigationItemSelectedListener(OnNavigationItemSelectedListener);

        Fragment checkout2 = new CheckoutDeliveryinfoFragment();



        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout,checkout2);
        fragmentTransaction.commit();
    }
    @Override
    public void onClick(View view) {
        if (view == back) {
            Intent n1=new Intent(Checkout2.this, MainActivity.class);
            startActivity(n1);
        }
    }
    private BottomNavigationView.OnNavigationItemSelectedListener OnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.bot_nav_home:
                    Intent n1=new Intent(Checkout2.this, MainActivity.class);
                    startActivity(n1);
                    return true;
                case R.id.bot_nav_allcategory:
                    Intent n2=new Intent(Checkout2.this, AllcategoryActivity.class);
                    startActivity(n2);
                    return true;
                case R.id.bot_nav_wishlist:
                    Intent n3=new Intent(Checkout2.this, WishlistActivity.class);
                    startActivity(n3);
                    return true;
                case R.id.bot_nav_orders:
                    Intent n4=new Intent(Checkout2.this, OrdersActivity.class);
                    startActivity(n4);
                    return true;
                case R.id.bot_nav_account:
                    Intent n5=new Intent(Checkout2.this, AccountActivity.class);
                    startActivity(n5);
                    return true;
            }
            return false;
        }
    };

}