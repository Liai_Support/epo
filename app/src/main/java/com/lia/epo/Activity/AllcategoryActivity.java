package com.lia.epo.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.lia.epo.Adapter.AllcategoryAdapter;
import com.lia.epo.DataModel.AllcategoryDataModel;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AllcategoryActivity extends AppCompatActivity implements View.OnClickListener {
    private static RecyclerView.Adapter adapter;
    ImageView back;
    static boolean active = true;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<AllcategoryDataModel> data;
    static View.OnClickListener myOnClickListener;
    TextView textCartItemCount;
    private Session session;
    public ArrayList<String> cartsku = new ArrayList<>();
  //  private String URLstringcartlist="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_allcategory);
        session = new Session(this);
        StaticInfo.dialogstart(this,session.getlang());
        Toolbar toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(this);
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        bottomNavigationView.setSelectedItemId(R.id.bot_nav_allcategory);
        bottomNavigationView.setOnNavigationItemSelectedListener(OnNavigationItemSelectedListener);
       // getcartdata();
        getInitialValues();
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        /*data = new ArrayList<AllcategoryDataModel>();
        for (int i = 0; i < AllcategoryData.nameArray.length; i++) {
            data.add(new AllcategoryDataModel(
                    AllcategoryData.nameArray[i],
                    AllcategoryData.versionArray[i],
                    AllcategoryData.id_[i],
                    AllcategoryData.drawableArray[i]
            ));
        }*/

        /*adapter = new AllcategoryAdapter(data);
        recyclerView.setAdapter(adapter);*/
    }

    private void getInitialValues() {
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            StringRequest stringRequest = new StringRequest(Request.Method.GET,  Urls.Url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("strrrrsdfr", ">>" + response);
                    try{
                        JSONObject obj = new JSONObject(response);
                        JSONArray dataa = obj.getJSONArray("children_data");
                        data = new ArrayList<>();
                        for(int i=0;i < dataa.length();i++) {
                            JSONObject childobject = dataa.getJSONObject(i);
                             childobject.getString("name");
                             childobject.getInt("id");
                             AllcategoryDataModel allcategoryDataModel= new AllcategoryDataModel(childobject.getString("name"),childobject.getInt("id"),childobject.getInt("product_count"));
                             data.add(allcategoryDataModel);
                             setupAllcategoryData(data);
                           //  Log.d("teli", "" +childobject);
                             Log.d("teli11", String.valueOf(childobject.getString("name")));
                            Log.d("teli1", String.valueOf(childobject.getInt("id")));
                        }
                        StaticInfo.dialogend();
                    }
                    catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("VOLLEY", error.toString());
                    StaticInfo.dialogend();
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                   /* try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }*/
                    return null;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(stringRequest);
        }
    private void setupAllcategoryData(List<AllcategoryDataModel> data) {
        adapter=new AllcategoryAdapter(this,data,getApplicationContext());
        recyclerView.setAdapter(adapter);

    }
    private BottomNavigationView.OnNavigationItemSelectedListener OnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.bot_nav_home:
                    Intent n1=new Intent(AllcategoryActivity.this, MainActivity.class);
                    startActivity(n1);
                    return true;
                case R.id.bot_nav_allcategory:
                    if(active == false){
                        Intent n5=new Intent(AllcategoryActivity.this, AllcategoryActivity.class);
                        startActivity(n5);
                    }
                    return true;
                case R.id.bot_nav_wishlist:
                    Intent n3=new Intent(AllcategoryActivity.this, WishlistActivity.class);
                    startActivity(n3);
                    return true;
                case R.id.bot_nav_orders:
                    Intent n4=new Intent(AllcategoryActivity.this, OrdersActivity.class);
                    startActivity(n4);
                    return true;
                case R.id.bot_nav_account:
                    Intent n5=new Intent(AllcategoryActivity.this, AccountActivity.class);
                    startActivity(n5);
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onBackPressed() {
        Intent n1=new Intent(AllcategoryActivity.this, MainActivity.class);
        startActivity(n1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_app_bar, menu);
        final MenuItem menuItem = menu.findItem(R.id.cart);

        View actionView = menuItem.getActionView();
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });
        textCartItemCount.setText(""+session.getcartcount());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.cart) {
            Intent n4=new Intent(AllcategoryActivity.this, CartActivity.class);
            startActivity(n4);
            return true;
        }
        else if(id == R.id.notification){
            Toast.makeText(getApplicationContext(),"Clicked",Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view == back) {
            Intent n1=new Intent(AllcategoryActivity.this, MainActivity.class);
            startActivity(n1);
        }
    }

/*
    private void getcartdata() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLstringcartlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("cartresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    obj.getInt("items_count");
                    obj.getInt("items_qty");
                    session.setquoteid(obj.getInt("id"));

                    Log.d("jmkuhj",""+obj.getJSONArray("items"));

                    JSONArray item = obj.getJSONArray("items");
                    session.setcartcount(item.length());
                    textCartItemCount.setText(""+session.getcartcount());
                    Log.d("dcfvgbhn",""+item.length());
                    for(int i=0;i<item.length();i++){
                        JSONObject childobject = item.getJSONObject(i);
                        cartsku.add(childobject.getString("sku"));
                    }
                    Log.d("ghnjmk",""+cartsku);
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }
*/
}