package com.lia.epo.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.lia.epo.DataModel.AllcategoryDataModel;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SigninActivity extends AppCompatActivity implements View.OnClickListener {
    TextView signin, signup;
    View signin_view,signup_view;
    LinearLayout signinmain;
    private Session session;
    EditText username_signin,password_signin,username_signup,mailid_signup,password_signup,confpassword_signup;
    Button buttonsignin,buttonsignup;
    private String URLstringsignin="https://www.vdcprojects.xyz/epo/index.php/rest/V1/integration/customer/token";
    private String URLstringsignup="https://www.vdcprojects.xyz/epo/rest/V1/customers";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        session = new Session(this);
        if (!session.isNetwork() == true){
            session.nonetwork();
        }
        Log.d("cfvgbhnjm",""+session.isNetwork());
        if (!session.getToken().equals("") && session.isNetwork() == true){
            Intent intent=new Intent(SigninActivity.this, MainActivity.class);
            startActivity(intent);
        }
        signin = (TextView) findViewById(R.id.txt_signin);
        signup = (TextView) findViewById(R.id.txt_signup);
        signin_view = (View) findViewById(R.id.signin_view);
        signup_view = (View) findViewById(R.id.signup_view);
        username_signin = (EditText)findViewById(R.id.edit_username_signin);
        password_signin = (EditText)findViewById(R.id.edit_password_signin);
        username_signup = (EditText)findViewById(R.id.edit_username_signup);
        password_signup = (EditText)findViewById(R.id.edit_password_signup);
        mailid_signup = (EditText)findViewById(R.id.edit_mailid_signup);
        confpassword_signup= (EditText)findViewById(R.id.edit_confpassword_signup);
        signup.setOnClickListener(this);
        signin.setOnClickListener(this);
        signinmain = (LinearLayout) findViewById(R.id.signin_main);
        signinmain.setVisibility(View.VISIBLE);
        signin_view.setVisibility(View.VISIBLE);
        signin.setTextColor(getResources().getColor(R.color.green));
        //Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.ic_rectangle_234);
        //signin.setCompoundDrawablesWithIntrinsicBounds(null,null,null, img);
        buttonsignin = (Button)findViewById(R.id.button_signin);
        buttonsignin.setOnClickListener(this);
        buttonsignup = (Button)findViewById(R.id.button_signup);
        buttonsignup.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onClick(View view) {
        if (view == signin) {
            signin.setTextColor(getResources().getColor(R.color.green));
            signup.setTextColor(getResources().getColor(R.color.black));
            /*Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.ic_rectangle_234);
            signup.setCompoundDrawables(null,null,null,null);
            signin.setCompoundDrawablesWithIntrinsicBounds(null,null,null, img);*/
            signin_view.setVisibility(View.VISIBLE);
            signup_view.setVisibility(View.GONE);
            View v1 = findViewById(R.id.signin_main);
            v1.setVisibility(view.VISIBLE);
            View v2 = findViewById(R.id.signup_main);
            v2.setVisibility(view.GONE);
        }
        else if(view == signup){
            signin.setTextColor(getResources().getColor(R.color.black));
            signup.setTextColor(getResources().getColor(R.color.green));
           /* Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.ic_rectangle_234);
            signup.setCompoundDrawablesWithIntrinsicBounds(null,null,null, img);
            signin.setCompoundDrawables(null,null,null,null);*/
            signup_view.setVisibility(View.VISIBLE);
            signin_view.setVisibility(View.GONE);
            View v1 = findViewById(R.id.signup_main);
            v1.setVisibility(view.VISIBLE);
            View v2 = findViewById(R.id.signin_main);
            v2.setVisibility(view.GONE);
        }
        else if(view == buttonsignin){
            String emailid = ".*[@].*";
            String cnameStr= String.valueOf(username_signin.getText().toString());
            String cpswdStr= String.valueOf(password_signin.getText().toString());
           if(username_signin.getText().toString().trim().length()==0 || password_signin.getText().toString().trim().length()==0){
               username_signin.setError("Enter Username");
               password_signin.setError("Enter Password");
               Snackbar.make(view,"Please Enter Username and Password",Snackbar.LENGTH_SHORT).show();
               //snackbar.setActionTextColor(Color.BLUE);
               //snackbar.setText("Please enter username and password");
               //snackbar.setTextColor(Color.WHITE);
               //snackbar.setBackgroundTint(Color.RED);
               //snackbar.setAnimationMode(BaseTransientBottomBar.ANIMATION_MODE_SLIDE);
               //View sbview = snackbar.getView();
               //sbview.setBackgroundColor(Color.GRAY);
               //snackbar.show();
            }
           else if(!cnameStr.matches(emailid)){
               username_signin.setError("Enter Valid Mailid");
               Snackbar.make(view,"Please Enter Valid Mailid",Snackbar.LENGTH_SHORT).show();
           }
           else if((cpswdStr.length()<8)){
               password_signin.setError("Enter Password Correctly");
               Snackbar.make(view,"Password Length is incorrect",Snackbar.LENGTH_SHORT).show();
           }
           else {
               StaticInfo.dialogstart(this,session.getlang());
               JSONObject requestbody = new JSONObject();
               try {
                   requestbody.put("username", cnameStr);
                   requestbody.put("password", cpswdStr);
                   signinrequestJSON(requestbody);
               } catch (JSONException e) {
                   e.printStackTrace();
               }
            }
        }
        else if(view == buttonsignup){
            String specialcharreg = ".*[@#!$%^&+=].*";
            String uppercasereg = ".*[A-Z].*";
            String numberreg = ".*[0-9].*";
            String emailid = ".*[@].*";
            String unameStr = username_signup.getText().toString();
            String pswdStr = password_signup.getText().toString();
            String mailStr = mailid_signup.getText().toString();
            String confpswdStr = confpassword_signup.getText().toString();
            if(unameStr.length()==0 || mailStr.length()==0 || pswdStr.length()==0 || confpswdStr.length()==0){
                username_signup.setError("Enter Username");
                password_signup.setError("Enter Password");
                mailid_signup.setError("Enter Mailid");
                confpassword_signup.setError("Enter Password");
                Snackbar.make(view,"Please Enter All Fields",Snackbar.LENGTH_SHORT).show();
            }
            else if(!mailStr.matches(emailid)){
                mailid_signup.setError("Enter Valid Mailid");
                Snackbar.make(view,"Please Enter Valid Mailid",Snackbar.LENGTH_SHORT).show();
            }
            else if(!pswdStr.equals(confpswdStr)){
                password_signup.setError("Enter Password Correctly");
                confpassword_signup.setError("Enter Password Correctly");
                Snackbar.make(view,"Password & Confirm Password Mismatch",Snackbar.LENGTH_SHORT).show();
            }
            else if((pswdStr.length()<8) || (!pswdStr.matches(specialcharreg)) || (!pswdStr.matches(uppercasereg)) || (!pswdStr.matches(numberreg))){
                password_signup.setError("Password must have 8 Characters,One UpperCase,Numeric,Special Symbol");
                confpassword_signup.setError("Password must have 8 Characters,One UpperCase,Numeric,Special Symbol");
                Snackbar.make(view,"Password must have 8 Characters,One UpperCase,Numeric,Special Symbol",Snackbar.LENGTH_SHORT).show();
            }
            else {
                StaticInfo.dialogstart(this,session.getlang());
                Log.d("mail", ">>" +mailStr);
                Log.d("mailget", ">>" +session.getmailid());
                JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("firstname", unameStr);
                    requestbody.put("lastname", unameStr);
                    requestbody.put("email", mailStr);
                    JSONObject mainreqbdy = new JSONObject();
                    mainreqbdy.put("customer", requestbody);
                    mainreqbdy.put("password", pswdStr);
                    signuprequestJSON(mainreqbdy);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void signuprequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request1",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLstringsignup, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("signupresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    int id = obj.getInt("id");
                    Log.d("id--", ">>" +id);
                    session.setUserId(id);
                    View view = buttonsignup;
                    Snackbar snackbar = Snackbar.make(view,"Signup Success",Snackbar.LENGTH_INDEFINITE)
                            .setAction("Sign In", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    signup_view.setVisibility(View.GONE);
                                    signin_view.setVisibility(View.VISIBLE);
                                    View v1 = findViewById(R.id.signin_main);
                                    v1.setVisibility(view.VISIBLE);
                                    View v2 = findViewById(R.id.signup_main);
                                    v2.setVisibility(view.GONE);
                                }
                            });
                    snackbar.show();
                    signup_view.setVisibility(View.GONE);
                    signin_view.setVisibility(View.VISIBLE);
                    View v1 = findViewById(R.id.signin_main);
                    v1.setVisibility(view.VISIBLE);
                    View v2 = findViewById(R.id.signup_main);
                    v2.setVisibility(view.GONE);
                    StaticInfo.dialogend();
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json" );
                Log.d("param",""+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }
    private void signinrequestJSON(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLstringsignin, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String finalresponse= response.replace('"',' ');
                String res = finalresponse.replaceAll(" ","");
                Log.d("token", ">>" + finalresponse);
                Log.d("token", ">>" + res);
                if(response.toString().trim().length()!=0){
                    String token = response;
                    //Toast.makeText(getApplicationContext(),token,Toast.LENGTH_SHORT).show();
                    session.setToken(res);
                    View view = signin_view;
                    Snackbar.make(view,"SignIn Success",Snackbar.LENGTH_LONG).show();
                    Intent n1=new Intent(SigninActivity.this, MainActivity.class);
                    startActivity(n1);
                }
                else {
                    Toast.makeText(getApplicationContext(),"Invalid Details please register first",Toast.LENGTH_SHORT).show();
                    View view = findViewById(R.id.button_signin);
                    Snackbar snackbar = Snackbar.make(view,"Invalid Details please register",Snackbar.LENGTH_SHORT)
                            .setAction("retry", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    signup_view.setVisibility(View.VISIBLE);
                                    signin_view.setVisibility(View.GONE);
                                    View v1 = findViewById(R.id.signin_main);
                                    v1.setVisibility(view.GONE);
                                    View v2 = findViewById(R.id.signup_main);
                                    v2.setVisibility(view.VISIBLE);
                                }
                            });
                    snackbar.setActionTextColor(Color.RED);
                    View sbview = snackbar.getView();
                    TextView textView = (TextView) sbview.findViewById(R.id.txt_signup);
                    textView.setTextColor(Color.YELLOW);
                    sbview.setBackgroundColor(Color.GRAY);
                    snackbar.show();
                }
                StaticInfo.dialogend();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        /*{
            //This is for Headers If You Needed
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String cnameStr= String.valueOf(username_signin.getText().toString());
                String cpswdStr= String.valueOf(password_signin.getText().toString());
                params.put("username", cnameStr);
                params.put("password", cpswdStr);
                Log.e("VOLLEY2",""+params);
                return params;
            };
            //Pass Your Parameters here
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                String cnameStr= String.valueOf(username_signin.getText().toString());
                String cpswdStr= String.valueOf(password_signin.getText().toString());
                params.put("username", cnameStr);
                params.put("password", cpswdStr);
                Log.e("VOLLEY5",""+params);
                return params;
            }
        };*/
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }
}