package com.lia.epo.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.lia.epo.R;
import com.lia.epo.Session;

import java.util.ArrayList;
import java.util.Locale;

public class NewLanguage extends AppCompatActivity implements View.OnClickListener {
    private ArrayList<CountryItem> mCountryList;
    private CountryAdapter mAdapter;
    String langcode="en";
    private Session session;
    Button next;
  private Toast backToast;
  private long backPressedTime;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_language);
        session = new Session(this);

//        programmatically set status bar color as green
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

        if (!session.isNetwork() == true) {
            session.nonetwork();
        }

        next=(Button) findViewById(R.id.to_signin);
        next.setOnClickListener(this);

//        DROPDOWN TO SELECT LANGUAGE
        initList();
        Spinner spinnerCountries = findViewById(R.id.spinner_countries);
        mAdapter = new CountryAdapter(this, mCountryList);
        spinnerCountries.setAdapter(mAdapter);
        spinnerCountries.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                CountryItem clickedItem = (CountryItem) parent.getItemAtPosition(position);
                String clickedCountryName = clickedItem.getCountryName();

                if (clickedCountryName.equals("Arabic")){

                    langcode = "ar";
                    session.setlang("ar");

                }
                if (clickedCountryName.equals("English")){

                    langcode = "en";
                    session.setlang("en");

                        }
                if (clickedCountryName.equals("Choose Your Language")){
                    langcode = "";
                }


            }
       @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(getApplicationContext(),"Please select Language",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void initList() {
        mCountryList = new ArrayList<>();
        mCountryList.add(new CountryItem("Choose Your Language", R.drawable.ic_trash));
        mCountryList.add(new CountryItem("English", R.drawable.ic_trash));
        mCountryList.add(new CountryItem("Arabic", R.drawable.ic_trash));
//        mCountryList.add(new CountryItem("USA", R.drawable.ic_trash));
//        mCountryList.add(new CountryItem("Germany", R.drawable.ic_trash));
    }

    @Override
    public void onClick(View v) {
        if (v == next) {
            if (!session.isNetwork() == true) {
                session.nonetwork();
            }

            if (langcode.equals("ar")) {
                setAppLocale(langcode);
            }
            if (langcode.equals("en")){
                setAppLocale(langcode);
            }
           if(langcode.equals("")){
                Toast.makeText(getApplicationContext(),"Please select Language",Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void setAppLocale(String localeCode) {
        Resources res=getResources();
        DisplayMetrics dm=res.getDisplayMetrics();
        Configuration conf=res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }
        else{
            conf.locale=new Locale(localeCode.toLowerCase());
        }
        SharedPreferences.Editor editor=getSharedPreferences("Settings",MODE_PRIVATE).edit();
        editor.putString("My_Lang",localeCode);
        editor.apply();
        res.updateConfiguration(conf,dm);
        Intent n5=new Intent(NewLanguage.this, NewSplash2Activity.class);
        startActivity(n5);
    }

    @Override
    public void onBackPressed() {
        if (backPressedTime+2000>System.currentTimeMillis()) {
            backToast.cancel();
            super.onBackPressed();
            return;
        }
        else {
            backToast=Toast.makeText(getBaseContext(),"Press Back Again to Exit",Toast.LENGTH_SHORT);
            backToast.show();
        }
        backPressedTime=System.currentTimeMillis();
    }

    @Override
    protected void onStart() {
        super.onStart();
        next.setEnabled(true);
    }
}
