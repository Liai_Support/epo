package com.lia.epo.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomnavigation.LabelVisibilityMode;
import com.google.android.material.snackbar.Snackbar;
import com.lia.epo.Adapter.CartAdapter;
import com.lia.epo.Adapter.NewCartAdapter;
import com.lia.epo.Adapter.ProductlistAdapter;
import com.lia.epo.DataModel.CartDataModel;
import com.lia.epo.DataModel.ProductlistDataModel;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewCartActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView back;
    public Session session;
    public TextView orderamount, textCartItemCount, textCartItemCount1;
    Button procced_checkout;
    public String cartimgurl;
    private static ArrayList<CartDataModel> data;
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private String URLstringcartlist = "https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine";
    private String URLstringdeletecart = "https://www.vdcprojects.xyz/epo/index.php/rest//V1/carts/mine/items/";
    private static final String url2 = "https://www.vdcprojects.xyz/epo/index.php/rest/english/V1/products?searchCriteria[filter_groups][0][filters][0][field]=sku&searchCriteria[filter_groups][0][filters][0][value]=";
    private static final String imgurl = "https://www.vdcprojects.xyz/epo/pub/media/catalog/product";
    public int itemcount;
    public Double totalamount = 0.0;
    public int CartItemCount;
    public ImageView clear;
    private Toast backToast;
    private long backPressedTime;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_cart);
        session = new Session(this);
        StaticInfo.dialogstart(this, session.getlang());
        orderamount = (TextView) findViewById(R.id.order_amount);
        Toolbar toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        textCartItemCount = (TextView) findViewById(R.id.cart_badge1);
//        textCartItemCount1 = (TextView) findViewById(R.id.cart_badge1);
        procced_checkout = (Button) findViewById(R.id.proceed_checkout);
        procced_checkout.setOnClickListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
        getcartdata();

        back = (ImageView) findViewById(R.id.back_main);
        back.setOnClickListener(this);

        clear = (ImageView) findViewById(R.id.clear);
        clear.setOnClickListener(this);


    }


    private void getcartdata() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLstringcartlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("cartresponse", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    obj.getInt("items_count");
                    obj.getInt("items_qty");
                    JSONArray dataa = new JSONArray(obj.getString("items"));
                    session.setcartcount(dataa.length());


                    textCartItemCount.setText(" " + session.getcartcount() + " " + "items");
                    CartItemCount = session.getcartcount();


                    data = new ArrayList<>();
                    itemcount = dataa.length();
                    if (dataa.length() > 0) {
                        for (int i = 0; i < dataa.length(); i++) {
                            JSONObject childobject = dataa.getJSONObject(i);
                            childobject.getString("name");
                            childobject.getDouble("price");
                            childobject.getDouble("qty");
                            url2function(childobject.getString("sku"), childobject.getString("name"), childobject.getDouble("price"), childobject.getInt("item_id"), childobject.getInt("qty"));
                       /* CartDataModel cartDataModel= new CartDataModel(childobject.getString("sku"),childobject.getString("name"),childobject.getDouble("price"),childobject.getInt("item_id"),childobject.getInt("qty"));
                        data.add(cartDataModel);*/
                            //setupAllcategoryData(data);
                            //  Log.d("teli", "" +childobject);
                            Log.d("teli11", String.valueOf(childobject.getString("name")));
                            Log.d("teli1", String.valueOf(childobject.getInt("item_id")));
                        }
                    } else {
                        StaticInfo.dialogend();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                StaticInfo.dialogend();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " + session.getToken());
                Log.d("param", "" + params);
                Log.d("id", "" + id);
                Log.d("token", "" + tok);
                return params;
            }

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1", "" + stringRequest);
        requestQueue.add(stringRequest);
    }

    private void url2function(final String sku, final String name, final Double price, final int item_id, final int qty) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url2 + sku, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("url2response", ">>" + response);
                try {
                    JSONObject obj = new JSONObject(response);
                    JSONArray dataa = obj.getJSONArray("items");
                    Log.d("gvbhjnvghbj", "" + dataa.length());
                    // data = new ArrayList<>();
                    JSONObject childobject = dataa.getJSONObject(0);
                    childobject.getString("name");
                    JSONArray img = childobject.getJSONArray("media_gallery_entries");
                    JSONObject imgobj = img.getJSONObject(0);
                    imgobj.getString("file");
                    Log.d("imgurl", "" + imgurl + imgobj.getString("file"));
                    String image = imgurl + imgobj.getString("file");
                    Log.d("nfnvhurvh", "" + cartimgurl);

                    CartDataModel cartDataModel = new CartDataModel(image, sku, name, price, item_id, qty);
                    data.add(cartDataModel);
                    adapter = new NewCartAdapter(data, NewCartActivity.this, getApplicationContext());
                    recyclerView.setAdapter(adapter);
                    StaticInfo.dialogend();
                  ArrayList<CartDataModel> dataSet=data;

                    Log.d("check","checkkkkkkkkkkkkkkkkkkk");
//--------------------------------------------------------------------------------------------------------------------------------------------------------
                    SwipeHelper swipeHelper = new SwipeHelper(getApplicationContext(), recyclerView) {
                        @Override
                        public void instantiateUnderlayButton(RecyclerView.ViewHolder viewHolder, List<UnderlayButton> underlayButtons) {
                            underlayButtons.add(new SwipeHelper.UnderlayButton(
                                    getString(R.string.delete),
                                    0,
                                    Color.parseColor("#BD0040"),
                                    pos -> {
                                        Log.d("ttt","tttttttttttt");
                                        Log.d("-----",""+dataSet.get(pos).getItemid());

                                        StaticInfo.dialogstart(NewCartActivity.this,session.getlang());
                                        deletecartdata(dataSet.get(pos).getItemid());

                                    }
                            ));
                        }
                    };
                    swipeHelper.attachSwipe();
//                    new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
//                        @Override
//                        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
//                            // this method is called
//                            // when the item is moved.
//                            return false;
//                        }
//
//                        @Override
//                        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
//                            // this method is called when we swipe our item to right direction.
//                            // on below line we are getting the item at a particular position.
//                            final CartDataModel deletedCourse = data.get(viewHolder.getAdapterPosition());
//
//                            // below line is to get the position
//                            // of the item at that position.
//                            final int position = viewHolder.getAdapterPosition();
//
//                            // this method is called when item is swiped.
//                            // below line is to remove item from our array list.
//                            data.remove(viewHolder.getAdapterPosition());
//
//                            // below line is to notify our item is removed from adapter.
//                            adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
//
//                            // below line is to display our snackbar with action.
//                            Snackbar.make(recyclerView, deletedCourse.getName(), Snackbar.LENGTH_LONG).setAction("Undo", new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    // adding on click listener to our action of snack bar.
//                                    // below line is to add our item to array list with a position.
//                                    data.add(position, deletedCourse);
//
//                                    // below line is to notify item is
//                                    // added to our adapter class.
//                                    adapter.notifyItemInserted(position);
//                                }
//                            }).show();
//                        }
//                        // at last we are adding this
//                        // to our recycler view.
//                    }).attachToRecyclerView(recyclerView);
//
//                    // at last we are adding this
//                    // to our recycler view.

//  ============================================================================================              =======================================================
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                StaticInfo.dialogend();
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                   /* try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }*/
                return null;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }


    @Override
    public void onClick(View view) {
        if (view == back) {
            Log.d("back",">>>>>>>>>>>>>>>>>>>>>>>>>>>..main");
            Intent n1 = new Intent(NewCartActivity.this, MainActivity.class);
            startActivity(n1);
        }
        if (view == procced_checkout) {
            Intent n2 = new Intent(NewCartActivity.this, CheckoutActivity.class);
            n2.putExtra("itemcount", itemcount);
            n2.putExtra("totalamount", totalamount);
            startActivity(n2);
        }

//        } else {
//            Intent n2 = new Intent(NewCartActivity.this, CheckoutActivity.class);
//            n2.putExtra("itemcount", itemcount);
//            n2.putExtra("totalamount", totalamount);
//            startActivity(n2);
//        }
        if (view == clear) {
            if (data.size() > 0) {
                StaticInfo.dialogstart(this, session.getlang());
                for (int i = 0; i < data.size(); i++) {
                    deletecartdata(data.get(i).getItemid());
                }
                Intent n76 = new Intent(NewCartActivity.this, NewCartActivity.class);
                StaticInfo.dialogend();
                startActivity(n76);
            }
            if (data.size()==0){
                clear.setVisibility(View.GONE);



            }
        }


    }

    private void deletecartdata(int itemid) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, URLstringdeletecart+itemid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("deletecartresponse", ">>" + response);
                StaticInfo.dialogend();
                Intent n76 = new Intent(NewCartActivity.this, NewCartActivity.class);
               startActivity(n76);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
//        if (backPressedTime+2000>System.currentTimeMillis()) {
//            backToast.cancel();
//
//            super.onBackPressed();
//            return;
//        }
//        else {
//            backToast=Toast.makeText(getBaseContext(),"Press Back Again to Exit",Toast.LENGTH_SHORT);
//            backToast.show();
//        }
//
//        backPressedTime=System.currentTimeMillis();
//
//    }
        Intent u=new Intent(NewCartActivity.this,MainActivity.class);
        startActivity(u);

    }
}

