package com.lia.epo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

public class StaticInfo {
    public static String d = String.valueOf(R.string.pleasewait);
    public static ProgressDialog progressDialog;
    public static String MAIN_URL="";
    public static String otpvalue="";
    public static boolean credit=false;
    public static int userId=0;
    public static int selProdId=0;
    public static int cartCount=0;
    public static int vatPrice=0;
//   public static String cartPrice="0";
    public static String mobNo="";
    public static Double cartPrice= Double.valueOf(0);
    public static Double cartTotal= Double.valueOf(0);

    public static boolean isValidMail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public static String getLoginUrl(String mobile, String password){

        String finalUrl=MAIN_URL+"driverlogin";
        finalUrl+="&mobile="+mobile;
        finalUrl+="&password="+password;
        return finalUrl;
    }

    public static boolean validEmail(String email) {
        // editing to make requirements listed
        // return email.matches("[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}");
        return email.matches("[A-Z0-9._%+-][A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{3}");
    }

    public static boolean mobile(String val){
        try{
            Long.parseLong(val);
            if(val.length()==10){
                return true;
            }
            else{
                return false;
            }
        }
        catch (Exception e){
            return false;
        }
    }

    public static void dialogstart(Activity mainActivity,String lang){
        if(lang.equals("ar")){
            Context context = mainActivity;
            Log.d("cfvgbhn", "" + d);
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("");
            progressDialog.setMessage("Please Wait!");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
        else {
            Context context = mainActivity;
            Log.d("cfvgbhn", "" + d);
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("");
            progressDialog.setMessage("Please Wait!");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }



    public static void dialogend(){
        progressDialog.dismiss();
    }


}
