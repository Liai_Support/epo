package com.lia.epo.DataModel;

public class AllcategoryDataModel {

    String name;
    String version;
    int id;
    int product_count;
    int image;

    public AllcategoryDataModel(String name,int id,int product_count) {
        this.name = name;
        this.version = version;
        this.id = id;
        this.product_count = product_count;
        this.image=image;
    }
    public String getName() {
        return name;
    }

    public int getProduct_count(){
        return product_count;
    }

    public String getVersion() {
        return version;
    }

    public int getImage() {
        return image;
    }

    public int getId() {
        return id;
    }
}
