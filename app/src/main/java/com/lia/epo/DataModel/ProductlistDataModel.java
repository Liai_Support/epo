package com.lia.epo.DataModel;

public class ProductlistDataModel {

    String name;
    int id,qty;
    String image;
    Double price;
    String sku;

    public ProductlistDataModel(String image,String sku,String name, int id, Double price) {
        this.name = name;
        this.price = price;
        this.id = id;
        this.sku = sku;
        this.qty = qty;
        this.image=image;
    }

    public String getSku(){
        return sku;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public int getId() {
        return id;
    }

    public int getQty(){
        return qty;
    }
}
