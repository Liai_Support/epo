package com.lia.epo.DataModel;

public class CartDataModel {

    String name,sku,image;
    int itemid,qty;

    Double price;

    public CartDataModel(String image,String sku,String name, Double price, int itemid,int qty) {
        this.name = name;
        this.sku = sku;
        this.price = price;
        this.itemid = itemid;
        this.qty = qty;
        this.image = image;
    }

    public String getSku(){
        return sku;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }

    public int getItemid() {
        return itemid;
    }

    public int getQty(){
        return qty;
    }
}
