package com.lia.epo.Adapter;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lia.epo.Activity.MainActivity;
import com.lia.epo.Activity.ProductlistActivity;
import com.lia.epo.DataModel.AllcategoryDataModel;
import com.lia.epo.R;

import java.util.ArrayList;

public class HomeCategoryAdapter extends RecyclerView.Adapter<HomeCategoryAdapter.MyViewHolder> {

    private ArrayList<AllcategoryDataModel> dataSet;
    MainActivity mainActivity;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewVersion;
        ImageView imageViewIcon;
        CardView cardviewcategery;
        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.name);
            this.cardviewcategery = (CardView) itemView.findViewById(R.id.cardview_categery);
            //this.textViewVersion = (TextView) itemView.findViewById(R.id.price);
            //this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
        }
    }

    public HomeCategoryAdapter(ArrayList<AllcategoryDataModel> data, MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_home_category, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        TextView textViewVersion = holder.textViewVersion;
        ImageView imageView = holder.imageViewIcon;

        textViewName.setText(dataSet.get(listPosition).getName());
        //textViewVersion.setText(dataSet.get(listPosition).getVersion());
        //imageView.setImageResource(dataSet.get(listPosition).getImage());
        holder.cardviewcategery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mainActivity, ProductlistActivity.class);
                intent.putExtra("categeryid",dataSet.get(listPosition).getId());
                intent.putExtra("categoryname",dataSet.get(listPosition).getName());
                intent.putStringArrayListExtra("cartlist",mainActivity.cartsku);
                mainActivity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}