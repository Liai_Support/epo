package com.lia.epo.Adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lia.epo.DataModel.CartDataModel;
import com.lia.epo.DataModel.OrderSummaryDataModel;
import com.lia.epo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class OrderSummaryAdapter extends RecyclerView.Adapter<OrderSummaryAdapter.MyViewHolder> {

    private ArrayList<CartDataModel> dataSet;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewprice,textViewqty;
        ImageView imageViewIcon;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.name);
            this.textViewprice = (TextView) itemView.findViewById(R.id.price);
            this.textViewqty = (TextView) itemView.findViewById(R.id.qty);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
        }
    }

    public OrderSummaryAdapter(ArrayList<CartDataModel> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_ordersummury, parent, false);

        //view.setOnClickListener(AllcategoryActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        TextView textViewprice = holder.textViewprice;
        ImageView imageView = holder.imageViewIcon;

        textViewName.setText(dataSet.get(listPosition).getName());
        holder.textViewprice.setText(""+dataSet.get(listPosition).getPrice()+" SAR");
        holder.textViewqty.setText(""+dataSet.get(listPosition).getQty());
        Picasso.get().load(dataSet.get(listPosition).getImage()).into(holder.imageViewIcon);
        //imageView.setImageResource(dataSet.get(listPosition).getImage());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}