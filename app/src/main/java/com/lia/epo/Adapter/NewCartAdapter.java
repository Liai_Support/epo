package com.lia.epo.Adapter;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.epo.Activity.CartActivity;
//import com.lia.epo.Activity.NewCartActivity;
import com.lia.epo.Activity.NewCartActivity;
import com.lia.epo.Activity.OrdersActivity;
import com.lia.epo.DataModel.CartDataModel;
import com.lia.epo.R;
import com.lia.epo.StaticInfo;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class NewCartAdapter extends RecyclerView.Adapter<NewCartAdapter.MyViewHolder> {

    private NewCartActivity newCartActivity;
    private ArrayList<CartDataModel> dataSet;

    Context context;
    boolean isplus,isminus;
    Double total=0.0;
    int quantity_count;
    int subqty;
    Double subprice,subtotal;
    private String URLstringdeletecart="https://www.vdcprojects.xyz/epo/index.php/rest//V1/carts/mine/items/";
    private String URLstringupdatecart = "https://www.vdcprojects.xyz/epo/index.php/rest//V1/carts/mine/items/";


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewprice;
        ImageView imageViewIcon;
        ImageView plus,minus;
        TextView cart_quantity;
        ImageView check;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.name);
            this.textViewprice = (TextView) itemView.findViewById(R.id.price);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
            this.plus = (ImageView) itemView.findViewById(R.id.plus_cart);
            this.minus = (ImageView) itemView.findViewById(R.id.minus_cart);
            this.cart_quantity = (TextView) itemView.findViewById(R.id.cart_quantity);
            this.check = (ImageView) itemView.findViewById(R.id.check);
//            this.deletecart = (ImageView) itemView.findViewById(R.id.delete_cart);
        }
    }

    public NewCartAdapter(ArrayList<CartDataModel> data, NewCartActivity newcartActivity, Context context) {
        this.dataSet = data;
      this.newCartActivity=newcartActivity;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.your_chart_listitem, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        final TextView textViewName = holder.textViewName;
        TextView textViewprice = holder.textViewprice;
        ImageView imageView = holder.imageViewIcon;
        final ImageView plus = holder.plus;
        final ImageView minus = holder.minus;
        final TextView cart_quantity = holder.cart_quantity;

        textViewName.setText(""+dataSet.get(listPosition).getName());
        textViewprice.setText(""+dataSet.get(listPosition).getPrice()+" SAR");
        holder.cart_quantity.setText((""+dataSet.get(listPosition).getQty()));
        quantity_count = Integer.parseInt(holder.cart_quantity.getText().toString());

        Picasso.get().load(dataSet.get(listPosition).getImage()).into(holder.imageViewIcon);
        //imageView.setImageResource(dataSet.get(listPosition).getImage());

//        holder.deletecart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                StaticInfo.dialogstart(newCartActivity,newCartActivity.session.getlang());
//                deletecartdata(dataSet.get(listPosition).getItemid());
//            }
//        });
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isplus = true;
                isminus = false;
                int count = Integer.parseInt(holder.cart_quantity.getText().toString());
                Log.d("8888888888888888888","88"+cart_quantity.getText().toString());
                count = count + 1;
                holder.cart_quantity.setText(String.valueOf(count));
                quantity_count = count;

                JSONObject mainbody = new JSONObject();
                JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("item_id", dataSet.get(listPosition).getItemid());
                    requestbody.put("qty", quantity_count);
                    requestbody.put("quote_id", newCartActivity.session.getquoetid());
                    mainbody.put("cartItem",requestbody);
                    updatecartdata(mainbody,dataSet.get(listPosition).getItemid());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isplus = false;
                isminus = true;
                int count = Integer.parseInt(holder.cart_quantity.getText().toString());
                if(count > 1) {
                    count = count - 1;
                    holder.cart_quantity.setText(String.valueOf(count));
                    quantity_count=count;

                    JSONObject mainbody = new JSONObject();
                    JSONObject requestbody = new JSONObject();
                    try {
                        requestbody.put("item_id", dataSet.get(listPosition).getItemid());
                        requestbody.put("qty", quantity_count);
                        requestbody.put("quote_id", newCartActivity.session.getquoetid());
                        mainbody.put("cartItem",requestbody);
                        updatecartdata(mainbody,dataSet.get(listPosition).getItemid());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
//        holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Log.d("xfcgvbhjn","stefcghjm");
//                if(holder.check.isChecked()){
//                    Double val1 = dataSet.get(listPosition).getPrice();
//                    int qt = Integer.parseInt(holder.cart_quantity.getText().toString());
//                    double val2 = val1*qt;
//                    cartActivity.orderamount.setText(""+val2);
//                }
//                else {
//                    cartActivity.orderamount.setText("");
//                }
//            }
//        });
        subprice = dataSet.get(listPosition).getPrice();
        subqty = dataSet.get(listPosition).getQty();
        subtotal = subprice*subqty;
        total = total+subtotal;
        newCartActivity.orderamount.setText(""+total+" SAR");
        newCartActivity.totalamount = total;
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    private void updatecartdata(JSONObject response,int itemid) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        final String requestBody = response.toString();
        Log.d("request1",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, URLstringupdatecart+itemid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("updatecartresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    if(isplus == true){
                        int cartitemcount =newCartActivity.CartItemCount;

                        Log.d("test44","4444444"+cartitemcount);
                        cartitemcount++;
                        // cartActivity.textCartItemCount.setText(""+cartitemcount);
                        //  cartActivity.session.setcartcount(cartitemcount);
                        Double ftotal = total+obj.getInt("price");
                        total = ftotal;
                        newCartActivity.totalamount = ftotal;
                        newCartActivity.orderamount.setText(""+ftotal+" SAR");
                    }
                    else {
                        // Double ftotal = Double.parseDouble(cartActivity.orderamount.getText().toString());
                        int cartitemcount = newCartActivity.CartItemCount;
                        cartitemcount--;
                        // cartActivity.textCartItemCount.setText(""+cartitemcount);
                        // cartActivity.session.setcartcount(cartitemcount);
                        Double ftotal = total-obj.getInt("price");
                        total = ftotal;
                        newCartActivity.totalamount = ftotal;
                        newCartActivity.orderamount.setText(""+ftotal+" SAR");
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,"some api isssue",Toast.LENGTH_SHORT).show();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +newCartActivity.session.getToken());
                Log.d("param",""+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }



    private void deletecartdata(int itemid) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, URLstringdeletecart+itemid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("deletecartresponse", ">>" + response);
                StaticInfo.dialogend();
                Intent n76 = new Intent(newCartActivity, CartActivity.class);
                newCartActivity.startActivity(n76);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
                StaticInfo.dialogend();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +newCartActivity.session.getToken());
                Log.d("param",""+params);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }
}