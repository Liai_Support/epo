package com.lia.epo.Adapter;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lia.epo.DataModel.HomeDataModel;
import com.lia.epo.R;

import java.util.ArrayList;

public class HomeBannerAdapter extends RecyclerView.Adapter<HomeBannerAdapter.MyViewHolder> {

    private ArrayList<HomeDataModel> dataSet;
    private final int[] backgroundColors = {Color.parseColor("#78bf37"),Color.parseColor("#5f3a17"),Color.parseColor("#069f9f")};

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewVersion;
        ImageView imageViewIcon;
        CardView cardView;
        LinearLayout linearLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            //this.textViewName = (TextView) itemView.findViewById(R.id.name);
            //this.textViewVersion = (TextView) itemView.findViewById(R.id.price);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
            this.cardView = (CardView) itemView.findViewById(R.id.cardview);
            this.linearLayout = (LinearLayout) itemView.findViewById(R.id.top);
        }
    }

    public HomeBannerAdapter(ArrayList<HomeDataModel> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_home_banner, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        TextView textViewVersion = holder.textViewVersion;
        ImageView imageView = holder.imageViewIcon;
        CardView cardView = holder.cardView;
        LinearLayout linearLayout = holder.linearLayout;

        int index = listPosition % backgroundColors.length;
        int color = backgroundColors[index];
        //holder.cardView.setCardBackgroundColor(color);
        holder.linearLayout.setBackgroundColor(color);

        //textViewName.setText(dataSet.get(listPosition).getName());
        //textViewVersion.setText(dataSet.get(listPosition).getVersion());
        imageView.setImageResource(dataSet.get(listPosition).getImage());
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}