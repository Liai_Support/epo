package com.lia.epo.Adapter;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.epo.Activity.ProductDetailsActivity;
import com.lia.epo.Activity.ProductlistActivity;
import com.lia.epo.DataModel.ProductDataModel;
import com.lia.epo.DataModel.ProductlistDataModel;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ProductlistAdapter extends RecyclerView.Adapter<ProductlistAdapter.MyViewHolder> {
    private Context mcontext;
    public ProductlistActivity productlistActivity;
    private ArrayList<ProductlistDataModel> dataSet;
    boolean iscart;
    int i;
    int wishlist_color=0,cart_color=0;
    private Session session;
    private String addtocarturl="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine/items";
    private String URLstringdeletecart="https://www.vdcprojects.xyz/epo/index.php/rest//V1/carts/mine/items/";
    private String URLstringcartlist="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine";
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewprice;
        ImageView imageViewIcon,icon_wishlist,icon_cart;
        TextView shopnow;
        CardView cardView;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.name);
            this.textViewprice = (TextView) itemView.findViewById(R.id.price);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
            this.icon_wishlist = (ImageView) itemView.findViewById(R.id.icon_wishlish);
            this.icon_cart = (ImageView) itemView.findViewById(R.id.icon_cart);
            this.shopnow = (TextView) itemView.findViewById(R.id.shopnow);
            this.cardView = (CardView) itemView.findViewById(R.id.cardview_product);
        }
    }

    public ProductlistAdapter(Context context, ArrayList<ProductlistDataModel> data,ProductlistActivity productlistActivity) {
        this.dataSet = data;
        this.mcontext = context;
        this.productlistActivity = productlistActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_productlist, parent, false);

        /*view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(productlistActivity, ProductDetailsActivity.class);
                intent.putExtra("sku",dataSet.get(listPosition).getSku());
                productlistActivity.startActivity(intent);
            }
        });*/
        session = new Session(mcontext);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        final TextView textViewName = holder.textViewName;
        //TextView textViewVersion = holder.textViewVersion;
        ImageView imageView = holder.imageViewIcon;
        final ImageView icon_wishlist = holder.icon_wishlist;
        final ImageView icon_cart = holder.icon_cart;
        TextView shopnow = holder.shopnow;

        textViewName.setText(dataSet.get(listPosition).getName());
        holder.textViewprice.setText(""+dataSet.get(listPosition).getPrice()+" SAR");
        Picasso.get().load(dataSet.get(listPosition).getImage()).into(holder.imageViewIcon);
        //textViewVersion.setText(dataSet.get(listPosition).getVersion());
        //imageView.setImageResource(dataSet.get(listPosition).getImage());

        for(int i=0;i<productlistActivity.cartsku.size();i++){
            if(dataSet.get(listPosition).getSku().equals(productlistActivity.cartsku.get(i))){
                Log.d("cartisnot null","cart value");
                icon_cart.setColorFilter(Color.parseColor("#8dc542"));
                //holder.textViewName.setText("hari");
            }
        }
        icon_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int n=2;
                int h=R.color.red;
                int t= Color.RED;
                if((wishlist_color % 2) == 0){
                    icon_wishlist.setColorFilter(Color.parseColor("#ff3939"));
                    Toast.makeText(mcontext,"Item Added to Wishlist",Toast.LENGTH_SHORT).show();
                    wishlist_color=wishlist_color+1;
                }
                else {
                    icon_wishlist.setColorFilter(null);
                    Toast.makeText(mcontext,"Item Removed from Wishlist",Toast.LENGTH_SHORT).show();
                    wishlist_color++;
                }
            }
        });
        icon_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // getcartdata(dataSet.get(listPosition).getSku());
                StaticInfo.dialogstart(productlistActivity,session.getlang());
                getcartdata(dataSet.get(listPosition).getSku(),holder.icon_cart);
                /*Log.d("dvfbn",""+iscart);
                if(iscart == false){
                    Toast.makeText(mcontext,"Item Removed from Cart",Toast.LENGTH_SHORT).show();
                    holder.icon_cart.setColorFilter(null);
                }
                else if(iscart == true){
                    Toast.makeText(mcontext,"Item Added to Cart",Toast.LENGTH_SHORT).show();
                    holder.icon_cart.setColorFilter(Color.parseColor("#8dc542"));
                }*/
               /* if((cart_color % 2) == 0){
                    icon_cart.setColorFilter(Color.parseColor("#8dc542"));
                    Toast.makeText(mcontext,"Item Added to Cart",Toast.LENGTH_SHORT).show();
                    cart_color=cart_color+1;
                    getcartdata(dataSet.get(listPosition).getSku());
                }
                else {
                    icon_cart.setColorFilter(null);
                    Toast.makeText(mcontext,"Item Removed from Cart",Toast.LENGTH_SHORT).show();
                    cart_color++;
                    getcartdata(dataSet.get(listPosition).getSku());
                }*/
            }
        });
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(productlistActivity, ProductDetailsActivity.class);
                intent.putExtra("sku",dataSet.get(listPosition).getSku());
                productlistActivity.startActivity(intent);
            }
        });
        shopnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(productlistActivity, ProductDetailsActivity.class);
                intent.putExtra("sku",dataSet.get(listPosition).getSku());
                productlistActivity.startActivity(intent);
            }
        });
    }

    private void addtocart(JSONObject response, final ImageView icon_cart,String sku) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        final String requestBody = response.toString();
        Log.d("request",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  addtocarturl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("addtocart", ">>" + response);
                if(!response.equals("")) {
                    Toast.makeText(mcontext, "Item Added to Cart", Toast.LENGTH_SHORT).show();
                    icon_cart.setColorFilter(Color.parseColor("#8dc542"));
                    int newcartcount = session.getcartcount() + 1;
                    session.setcartcount(newcartcount);
                    productlistActivity.textCartItemCount.setText("" + newcartcount);
                }
                StaticInfo.dialogend();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                icon_cart.setColorFilter(null);
                Toast.makeText(mcontext,"Some Issue on api",Toast.LENGTH_SHORT).show();
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }

    private void deletecartdata(int itemid, final ImageView icon_cart,String sku) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, URLstringdeletecart+itemid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("deletecartresponse", ">>" + response);
                if(!response.equals("")) {
                    icon_cart.setColorFilter(null);
                    Toast.makeText(mcontext, "Item Removed from Cart", Toast.LENGTH_SHORT).show();
                    int newcartcount = session.getcartcount() - 1;
                    session.setcartcount(newcartcount);
                    productlistActivity.textCartItemCount.setText("" + newcartcount);
                }
                StaticInfo.dialogend();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                icon_cart.setColorFilter(Color.parseColor("#8dc542"));
                Toast.makeText(mcontext, "some api issue", Toast.LENGTH_SHORT).show();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void getcartdata(final String sku, final ImageView icon_cart) {
        final RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLstringcartlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("cartresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    JSONArray item = obj.getJSONArray("items");
                    Log.d("dcfvgbhn",""+item.length());
                    if(item.length()>0){
                        for (i = 0; i < item.length(); i++) {
                            JSONObject childobject = item.getJSONObject(i);
                            if (childobject.getString("sku").equals(sku)) {
                                deletecartdata(childobject.getInt("item_id"),icon_cart,sku);
                                iscart = true;
                                break;
                            }
                            else{
                                iscart = false;
                            }
                        }
                        if(iscart == false){
                            //iscart = true;
                            JSONObject requestbody = new JSONObject();
                            try {
                                requestbody.put("sku", sku);
                                requestbody.put("qty", 1);
                                requestbody.put("quote_id", String.valueOf(session.getquoetid()));
                                JSONObject mainreqbdy = new JSONObject();
                                mainreqbdy.put("cartItem", requestbody);
                                addtocart(mainreqbdy,icon_cart,sku);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else {
                        //iscart = true;
                        JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("sku", sku);
                            requestbody.put("qty", 1);
                            requestbody.put("quote_id", String.valueOf(session.getquoetid()));
                            JSONObject mainreqbdy = new JSONObject();
                            mainreqbdy.put("cartItem", requestbody);
                            addtocart(mainreqbdy,icon_cart,sku);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext,"Some api issue",Toast.LENGTH_SHORT).show();
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}