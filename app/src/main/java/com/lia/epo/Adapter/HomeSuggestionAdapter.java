package com.lia.epo.Adapter;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.epo.Activity.MainActivity;
import com.lia.epo.Activity.ProductDetailsActivity;
import com.lia.epo.DataModel.ProductlistDataModel;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class HomeSuggestionAdapter extends RecyclerView.Adapter<HomeSuggestionAdapter.MyViewHolder> {
    Context mcontext;
    MainActivity mainActivity;
    private ArrayList<ProductlistDataModel> dataSet;
    boolean iscart;
    int i;
    public ProgressDialog progressDialog;
    private Session session;
    private String addtocarturl="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine/items";
    private String URLstringdeletecart="https://www.vdcprojects.xyz/epo/index.php/rest//V1/carts/mine/items/";
    private String URLstringcartlist="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine";
    int wishlist_color=0,cart_color=0;
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewPrice,shopnow;
        CardView cardView;
        public ImageView imageViewIcon,icon_wishlist,icon_cart;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.name);
            this.textViewPrice = (TextView) itemView.findViewById(R.id.price);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
            this.icon_wishlist = (ImageView) itemView.findViewById(R.id.icon_wishlish);
            this.icon_cart = (ImageView) itemView.findViewById(R.id.icon_cart);
            this.cardView = (CardView) itemView.findViewById(R.id.cardview_suggestion);
            this.shopnow = (TextView) itemView.findViewById(R.id.shop_now);
        }
    }

    public HomeSuggestionAdapter(Context context,ArrayList<ProductlistDataModel> data,MainActivity mainActivity) {
        this.dataSet = data;
        this.mcontext = context;
        this.mainActivity = mainActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_home_suggestion, parent, false);

       /* view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mcontext, ProductDetailsActivity.class);
                mcontext.startActivity(intent);
            }
        });*/
        session = new Session(mcontext);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        final TextView textViewName = holder.textViewName;
        TextView textViewPrice = holder.textViewPrice;
        ImageView imageView = holder.imageViewIcon;
        final ImageView icon_wishlist = holder.icon_wishlist;
        final ImageView icon_cart = holder.icon_cart;

        textViewName.setText(""+dataSet.get(listPosition).getName());
        textViewPrice.setText(""+dataSet.get(listPosition).getPrice()+" SAR");
        //Log.d("rtyvgbhjnj",""+dataSet.get(listPosition).getImage());
       // imageView.setImageResource(""+dataSet.get(listPosition).getImage());
        Picasso.get().load(dataSet.get(listPosition).getImage()).into(holder.imageViewIcon);
       /* Log.d("cartsku",""+mainActivity.cartsku);
        Log.d("cartsku",""+mainActivity.cartsku.size());*/
        for(int i=0;i<mainActivity.cartsku.size();i++){
            if(dataSet.get(listPosition).getSku().equals(mainActivity.cartsku.get(i))){
                Log.d("cartisnot null","cart value");
                icon_cart.setColorFilter(Color.parseColor("#8dc542"));
                //holder.textViewName.setText("hari");
            }
        }

        icon_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int n=2;
                if((wishlist_color % 2) == 0){
                    icon_wishlist.setColorFilter(Color.parseColor("#ff3939"));
                    Log.d("hari",""+wishlist_color);
                    Toast.makeText(mcontext,"Item Added to Wishlist",Toast.LENGTH_SHORT).show();
                    wishlist_color++;
                }
                else {
                    icon_wishlist.setColorFilter(null);
                    Toast.makeText(mcontext,"Item Removed from Wishlist",Toast.LENGTH_SHORT).show();
                    wishlist_color++;
                }
            }
        });
        holder.icon_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // getcartdata(dataSet.get(listPosition).getSku());
                StaticInfo.dialogstart(mainActivity,session.getlang());
                getcartdata(dataSet.get(listPosition).getSku(),holder.icon_cart);
                /*Log.d("dvfbn",""+iscart);
                if(iscart == false){
                    Toast.makeText(mcontext,"Item Removed from Cart",Toast.LENGTH_SHORT).show();
                    holder.icon_cart.setColorFilter(null);
                }
                else if(iscart == true){
                    Toast.makeText(mcontext,"Item Added to Cart",Toast.LENGTH_SHORT).show();
                    holder.icon_cart.setColorFilter(Color.parseColor("#8dc542"));
                }*/
               /* if((cart_color % 2) == 0){
                    icon_cart.setColorFilter(Color.parseColor("#8dc542"));
                    Toast.makeText(mcontext,"Item Added to Cart",Toast.LENGTH_SHORT).show();
                    cart_color=cart_color+1;
                    getcartdata(dataSet.get(listPosition).getSku());
                }
                else {
                    icon_cart.setColorFilter(null);
                    Toast.makeText(mcontext,"Item Removed from Cart",Toast.LENGTH_SHORT).show();
                    cart_color++;
                    getcartdata(dataSet.get(listPosition).getSku());
                }*/
            }
        });
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mainActivity, ProductDetailsActivity.class);
                intent.putExtra("sku",dataSet.get(listPosition).getSku());
                mainActivity.startActivity(intent);
            }
        });
        holder.shopnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mainActivity, ProductDetailsActivity.class);
                intent.putExtra("sku",dataSet.get(listPosition).getSku());
                mainActivity.startActivity(intent);
            }
        });
    }

    private void addtocart(JSONObject response, final ImageView icon_cart,String sku) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        final String requestBody = response.toString();
        Log.d("request",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  addtocarturl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("addtocart", ">>" + response);
                if(!response.equals("")) {
                    Toast.makeText(mcontext, "Item Added to Cart", Toast.LENGTH_SHORT).show();
                    icon_cart.setColorFilter(Color.parseColor("#8dc542"));
                    int newcartcount = session.getcartcount() + 1;
                    session.setcartcount(newcartcount);
                    mainActivity.textCartItemCount.setText("" + newcartcount);
                    StaticInfo.dialogend();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                icon_cart.setColorFilter(null);
                Toast.makeText(mcontext,"Some Issue on api",Toast.LENGTH_SHORT).show();
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                   try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }

    private void deletecartdata(int itemid, final ImageView icon_cart,String sku) {
        RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, URLstringdeletecart+itemid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("deletecartresponse", ">>" + response);
                if(!response.equals("")) {
                    icon_cart.setColorFilter(null);
                    Toast.makeText(mcontext, "Item Removed from Cart", Toast.LENGTH_SHORT).show();
                    int newcartcount = session.getcartcount() - 1;
                    session.setcartcount(newcartcount);
                    mainActivity.textCartItemCount.setText("" + newcartcount);
                    StaticInfo.dialogend();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                icon_cart.setColorFilter(Color.parseColor("#8dc542"));
                Toast.makeText(mcontext, "some api issue", Toast.LENGTH_SHORT).show();
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void getcartdata(final String sku, final ImageView icon_cart) {
        final RequestQueue requestQueue = Volley.newRequestQueue(mcontext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLstringcartlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("cartresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    JSONArray item = obj.getJSONArray("items");
                    Log.d("dcfvgbhn",""+item.length());
                    if(item.length()>0){
                        for (i = 0; i < item.length(); i++) {
                            JSONObject childobject = item.getJSONObject(i);
                            if (childobject.getString("sku").equals(sku)) {
                                deletecartdata(childobject.getInt("item_id"),icon_cart,sku);
                                iscart = true;
                                break;
                            }
                            else{
                                iscart = false;
                            }
                        }
                        if(iscart == false){
                            //iscart = true;
                            JSONObject requestbody = new JSONObject();
                            try {
                                requestbody.put("sku", sku);
                                requestbody.put("qty", 1);
                                requestbody.put("quote_id", String.valueOf(session.getquoetid()));
                                JSONObject mainreqbdy = new JSONObject();
                                mainreqbdy.put("cartItem", requestbody);
                                addtocart(mainreqbdy,icon_cart,sku);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    else {
                        //iscart = true;
                        JSONObject requestbody = new JSONObject();
                        try {
                            requestbody.put("sku", sku);
                            requestbody.put("qty", 1);
                            requestbody.put("quote_id", String.valueOf(session.getquoetid()));
                            JSONObject mainreqbdy = new JSONObject();
                            mainreqbdy.put("cartItem", requestbody);
                            addtocart(mainreqbdy,icon_cart,sku);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(mcontext,"Some api issue",Toast.LENGTH_SHORT).show();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}