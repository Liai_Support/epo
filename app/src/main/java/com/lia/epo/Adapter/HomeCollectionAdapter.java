package com.lia.epo.Adapter;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lia.epo.Activity.MainActivity;
import com.lia.epo.Activity.ProductDetailsActivity;
import com.lia.epo.DataModel.HomeDataModel;
import com.lia.epo.R;

import java.util.ArrayList;

public class HomeCollectionAdapter extends RecyclerView.Adapter<HomeCollectionAdapter.MyViewHolder> {
    private ArrayList<HomeDataModel> dataSet;
    Context mcontext;
    int wishlist_color=0;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView textViewName;
        TextView textViewVersion;
        ImageView imageViewIcon,icon_wishlist;

        public MyViewHolder(View itemView) {
            super(itemView);
            //this.textViewName = (TextView) itemView.findViewById(R.id.name);
            //this.textViewVersion = (TextView) itemView.findViewById(R.id.price);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
            this.icon_wishlist = (ImageView) itemView.findViewById(R.id.icon_wishlish);
        }
    }

    public HomeCollectionAdapter(Context context,ArrayList<HomeDataModel> data) {
        this.dataSet = data;
        mcontext = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_home_collection, parent, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mcontext,ProductDetailsActivity.class);
                mcontext.startActivity(intent);
            }
        });

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        TextView textViewVersion = holder.textViewVersion;
        ImageView imageView = holder.imageViewIcon;
        final ImageView icon_wishlist = holder.icon_wishlist;

        //textViewName.setText(dataSet.get(listPosition).getName());
        //textViewVersion.setText(dataSet.get(listPosition).getVersion());
        imageView.setImageResource(dataSet.get(listPosition).getImage());
        icon_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int n=2;
                if((wishlist_color % 2) == 0){
                    icon_wishlist.setColorFilter(Color.parseColor("#ff3939"));
                    Log.d("hari",""+wishlist_color);
                    Toast.makeText(mcontext,"Item Added to Wishlist",Toast.LENGTH_SHORT).show();
                    wishlist_color++;
                }
                else {
                    icon_wishlist.setColorFilter(null);
                    Toast.makeText(mcontext,"Item Removed from Wishlist",Toast.LENGTH_SHORT).show();
                    wishlist_color++;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}