package com.lia.epo.Adapter;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lia.epo.Activity.AllcategoryActivity;
import com.lia.epo.Activity.ProductlistActivity;
import com.lia.epo.DataModel.AllcategoryDataModel;
import com.lia.epo.R;

import java.util.List;

public class AllcategoryAdapter extends RecyclerView.Adapter<AllcategoryAdapter.MyViewHolder> {
    private Context mcontext;
    private List<AllcategoryDataModel> dataSet;
    AllcategoryActivity allcategoryActivity;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName,productcount;
        TextView textViewVersion;
        CardView cardviewcategery;
        ImageView imageViewIcon,next;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.textViewName = (TextView) itemView.findViewById(R.id.name);
            this.next = (ImageView) itemView.findViewById(R.id.next);
            this.productcount = (TextView) itemView.findViewById(R.id.product_count);
            this.cardviewcategery = (CardView) itemView.findViewById(R.id.cardview_categery);
           /* this.textViewVersion = (TextView) itemView.findViewById(R.id.price);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);*/
        }
    }

    public AllcategoryAdapter(AllcategoryActivity allcategoryActivity, List<AllcategoryDataModel> data, Context context) {
        this.dataSet = data;
        this.mcontext = context;
        this.allcategoryActivity = allcategoryActivity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_allcategory, parent, false);

        /*view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mcontext, ProductlistActivity.class);
                mcontext.startActivity(intent);
            }
        });*/

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int listPosition) {
        TextView textViewName = holder.textViewName;
        ImageView next = holder.next;
        /*TextView textViewVersion = holder.textViewVersion;
        ImageView imageView = holder.imageViewIcon;*/

        textViewName.setText(dataSet.get(listPosition).getName());
        holder.productcount.setText(""+dataSet.get(listPosition).getProduct_count()+" Products");
       /* textViewVersion.setText(dataSet.get(listPosition).getVersion());
        imageView.setImageResource(dataSet.get(listPosition).getImage());*/
       /*holder.next.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent = new Intent(allcategoryActivity, ProductlistActivity.class);
               intent.putExtra("categeryid",dataSet.get(listPosition).getId());
               intent.putExtra("categoryname",dataSet.get(listPosition).getName());
               intent.putStringArrayListExtra("cartlist",allcategoryActivity.cartsku);
               allcategoryActivity.startActivity(intent);
           }
       });*/
       holder.cardviewcategery.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent = new Intent(allcategoryActivity, ProductlistActivity.class);
               intent.putExtra("categeryid",dataSet.get(listPosition).getId());
               intent.putExtra("categoryname",dataSet.get(listPosition).getName());
               intent.putStringArrayListExtra("cartlist",allcategoryActivity.cartsku);
               allcategoryActivity.startActivity(intent);
           }
       });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}