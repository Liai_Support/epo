package com.lia.epo.Adapter;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lia.epo.DataModel.OrdersDataModel;
import com.lia.epo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductImageAdapter extends RecyclerView.Adapter<ProductImageAdapter.MyViewHolder> {

    private ArrayList<String> dataSet;
    Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewVersion;
        ImageView imageViewIcon,back;


        public MyViewHolder(View itemView) {
            super(itemView);
            //this.textViewName = (TextView) itemView.findViewById(R.id.name);
            //this.textViewVersion = (TextView) itemView.findViewById(R.id.price);
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
        }
    }

    public ProductImageAdapter(ArrayList<String> data, Context applicationContext) {
        this.dataSet = data;
        this.context = applicationContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_product_image, parent, false);

        //view.setOnClickListener(AllcategoryActivity.myOnClickListener);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView textViewName = holder.textViewName;
        TextView textViewVersion = holder.textViewVersion;
        ImageView imageView = holder.imageViewIcon;


        //textViewName.setText(dataSet.get(listPosition).getName());
        //textViewVersion.setText(dataSet.get(listPosition).getVersion());
        Log.d("dfdfghg",""+dataSet.size());
        Log.d("dfdfghg",""+dataSet.get(listPosition));
        String imgurl = dataSet.get(listPosition);
        Picasso.get().load(imgurl).into(holder.imageViewIcon);
        //imageView.setImageResource(imgurl);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}