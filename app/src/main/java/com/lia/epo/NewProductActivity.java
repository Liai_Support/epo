package com.lia.epo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.epo.Activity.CartActivity;
import com.lia.epo.Activity.MainActivity;
import com.lia.epo.Adapter.ProductlistAdapter;
import com.lia.epo.Data.OrdersData;
import com.lia.epo.DataModel.OrdersDataModel;
import com.lia.epo.Adapter.ProductReviewAdapter;
import com.lia.epo.Adapter.ProductImageAdapter;
import com.lia.epo.DataModel.ProductDataModel;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NewProductActivity extends AppCompatActivity implements View.OnClickListener{
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private LinearLayoutManager HorizontalLayout;
    private LinearLayout description,review;
    int wishlist_color=0,cart_color=0;
    String sku;
    TextView textCartItemCount;
    TextView txt_description,txt_review,txt_quantity_count,txt_productname,txt_productprice;
    ImageView plus,minus,back,icon_wishlist,icon_share,iconcart;
    private static RecyclerView recyclerView,productimagerecycler;
    private static ArrayList<OrdersDataModel> data;
    Button cartadd,shopnow;
    public ArrayList<String> productimage = new ArrayList<>();
    private Session session;
    boolean iscart = false;
    boolean isproductcountchange = false;
    static View.OnClickListener myOnClickListener;
    private String URLstringupdatecart = "https://www.vdcprojects.xyz/epo/index.php/rest//V1/carts/mine/items/";
    private String URLstringcartlist="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine";
    private String addtocarturl="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine/items";
    private static  final String imgurl = "https://www.vdcprojects.xyz/epo/pub/media/catalog/product";
    private static final String url2 = "https://www.vdcprojects.xyz/epo/index.php/rest/english/V1/products?searchCriteria[filter_groups][0][filters][0][field]=sku&searchCriteria[filter_groups][0][filters][0][value]=";
    int count=0,quantity_count=1;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        session = new Session(this);
        StaticInfo.dialogstart(this,session.getlang());
        sku = getIntent().getStringExtra("sku");
        Log.d("sku",""+sku);
        Toolbar toolbar = findViewById(R.id.appbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");
        back = (ImageView)findViewById(R.id.back);
        cartadd = (Button) findViewById(R.id.btn_addtocart);
        shopnow = (Button) findViewById(R.id.btn_shopnow);
        cartadd.setOnClickListener(this);
        shopnow.setOnClickListener(this);
        icon_wishlist = (ImageView)findViewById(R.id.icon_wishlish);
        iconcart = (ImageView)findViewById(R.id.icon_cart);
        back.setOnClickListener((View.OnClickListener) this);
        icon_wishlist.setOnClickListener(this);
        iconcart.setOnClickListener(this);
        txt_productname = (TextView) findViewById(R.id.product_name);
        txt_productprice = (TextView) findViewById(R.id.product_price);
        txt_description = (TextView)findViewById(R.id.txt_description);
        txt_review = (TextView)findViewById(R.id.txt_review);
        description = (LinearLayout)findViewById(R.id.description);
        review = (LinearLayout)findViewById(R.id.review);
        txt_description.setOnClickListener(this);
        txt_review.setOnClickListener(this);
        txt_quantity_count =(TextView)findViewById(R.id.txt_quantity_count);
        plus = (ImageView)findViewById(R.id.plus);
        minus = (ImageView)findViewById(R.id.minus);
        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        txt_quantity_count.setText(String.valueOf(quantity_count));
        productimagerecycler = (RecyclerView) findViewById(R.id.recycler);
        productimagerecycler.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        HorizontalLayout = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        productimagerecycler.setLayoutManager(HorizontalLayout);
        productimagerecycler.setItemAnimator(new DefaultItemAnimator());



       /* data = new ArrayList<OrdersDataModel>();
        for (int i = 0; i < OrdersData.nameArray.length; i++) {
            data.add(new OrdersDataModel(
                    OrdersData.nameArray[i],
                    OrdersData.versionArray[i],
                    OrdersData.id_[i],
                    OrdersData.drawableArray[i]
            ));
        }
*/


        getcartdata();
        productdetails(sku);
        recyclerView = (RecyclerView) findViewById(R.id.recycler1);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<OrdersDataModel>();
        for (int i = 0; i < OrdersData.nameArray.length; i++) {
            data.add(new OrdersDataModel(
                    OrdersData.nameArray[i],
                    OrdersData.versionArray[i],
                    OrdersData.id_[i],
                    OrdersData.drawableArray[i]
            ));
        }

        adapter = new ProductReviewAdapter(data);
        recyclerView.setAdapter(adapter);
    }


    private void productdetails(String sku) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,  url2+sku, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("url2response", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    JSONArray dataa = obj.getJSONArray("items");
                    JSONObject childobject2 = dataa.getJSONObject(0);
                    Log.d("gvbhjnvghbj",""+childobject2);
                    txt_productname.setText(""+childobject2.getString("name"));
                    txt_productprice.setText(""+childobject2.getString("price")+" $");
                    JSONArray img = childobject2.getJSONArray("media_gallery_entries");
                    for(int i=0;i<img.length();i++){
                        JSONObject imgobj = img.getJSONObject(i);
                        imgobj.getString("file");
                        Log.d("imgurl",""+imgurl+imgobj.getString("file"));
                        String image = imgurl+imgobj.getString("file");
                        productimage.add(image);
                    }
                    adapter = new ProductImageAdapter(productimage,getApplicationContext());
                    productimagerecycler.setAdapter(adapter);
                    StaticInfo.dialogend();
                  /*  // data = new ArrayList<>();
                    for(int i=0;i < dataa.length();i++) {
                        JSONObject childobject = dataa.getJSONObject(i);
                        childobject.getString("name");
                        JSONArray img = childobject.getJSONArray("media_gallery_entries");
                        JSONObject imgobj = img.getJSONObject(0);
                        imgobj.getString("file");
                        Log.d("imgurl",""+imgurl+imgobj.getString("file"));
                        String image = imgurl+imgobj.getString("file");
                       // productimage.add();
                       // ProductDataModel productDataModel= new ProductDataModel(image,childobject.getString("sku"),childobject.getString("name"),childobject.getInt("id"),childobject.getDouble("price"));
                       // data.add(productDataModel);
                        //recyclerView.setAdapter(new ProductlistAdapter(getApplicationContext(),data,ProductlistActivity.this));
                    }*/
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Toast.makeText(getApplicationContext(),"some api issue",Toast.LENGTH_SHORT).show();
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                   /* try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }*/
                return null;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getcartdata() {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLstringcartlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("cartresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    obj.getInt("items_count");
                    obj.getInt("items_qty");
                    session.setcartcount(obj.getInt("items_qty"));
                    textCartItemCount.setText(""+session.getcartcount());
                    JSONArray dataa = new JSONArray(obj.getString("items"));
                    data = new ArrayList<>();
                    for(int i=0;i < dataa.length();i++) {
                        JSONObject childobject = dataa.getJSONObject(i);
                        if (childobject.getString("sku").equals(sku)){
                            iconcart.setColorFilter(Color.parseColor("#8dc542"));
                            txt_quantity_count.setText(""+childobject.getInt("qty"));
                            quantity_count = childobject.getInt("qty");
                            iscart = true;
                        }
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Toast.makeText(getApplicationContext(),"some api issue",Toast.LENGTH_SHORT).show();
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_app_bar_cart_filter, menu);
        final MenuItem menuItem = menu.findItem(R.id.cart);

        View actionView = menuItem.getActionView();
        textCartItemCount = (TextView) actionView.findViewById(R.id.cart_badge);

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });
        textCartItemCount.setText(""+session.getcartcount());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.cart) {
            Intent n4=new Intent(NewProductActivity.this, CartActivity.class);
            startActivity(n4);
            return true;
        }
        else if(id == R.id.filter){
            Toast.makeText(getApplicationContext(),"Clicked",Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view == txt_description) {
            if(count == 0){
                View v1 = findViewById(R.id.description);
                v1.setVisibility(view.GONE);
                count++;
            }
            else{
                View v1 = findViewById(R.id.description);
                v1.setVisibility(view.VISIBLE);
                count=0;
            }
        }
        else if(view == txt_review){
            if(count == 0){
                View v1 = findViewById(R.id.review);
                v1.setVisibility(view.GONE);
                count++;
            }
            else{
                View v1 = findViewById(R.id.review);
                v1.setVisibility(view.VISIBLE);
                count=0;
            }
        }
        else if(view == plus){
            isproductcountchange = true;
            quantity_count=quantity_count+1;
            txt_quantity_count.setText(String.valueOf(quantity_count));
        }
        else if(view == minus){
            if(quantity_count>1) {
                isproductcountchange = true;
                quantity_count = quantity_count - 1;
                txt_quantity_count.setText(String.valueOf(quantity_count));
            }
        }
        else if (view == back) {
            Intent n1=new Intent(NewProductActivity.this, MainActivity.class);
            startActivity(n1);
        }
        else if (view == icon_wishlist) {
            int n=2;
            if((wishlist_color % 2) == 0){
                icon_wishlist.setColorFilter(Color.parseColor("#ff3939"));
                wishlist_color++;
            }
            else {
                icon_wishlist.setColorFilter(null);
                wishlist_color++;
            }
        }
        else if (view == iconcart) {
            /*int n=2;
            if((cart_color % 2) == 0){
                iconcart.setColorFilter(Color.parseColor("#8dc542"));
                cart_color=cart_color+1;
            }
            else {
                iconcart.setColorFilter(null);
                cart_color++;
            }*/
        }
        else if(view == cartadd){
            if(iscart == true){
                Toast.makeText(this,"item already in cart",Toast.LENGTH_SHORT).show();
                /*Intent intent = new Intent(ProductDetailsActivity.this,CartActivity.class);
                startActivity(intent);*/
            }
            else {
                StaticInfo.dialogstart(this,session.getlang());
                JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("sku", sku);
                    requestbody.put("qty", Integer.parseInt(txt_quantity_count.getText().toString()));
                    requestbody.put("quote_id", String.valueOf(session.getquoetid()));
                    JSONObject mainreqbdy = new JSONObject();
                    mainreqbdy.put("cartItem", requestbody);
                    addtocart(mainreqbdy);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(view == shopnow){
            if(iscart == true){
                Intent intent = new Intent(NewProductActivity.this,CartActivity.class);
                startActivity(intent);
            }
            else {
                StaticInfo.dialogstart(this,session.getlang());
                JSONObject requestbody = new JSONObject();
                try {
                    requestbody.put("sku", sku);
                    requestbody.put("qty", Integer.parseInt(txt_quantity_count.getText().toString()));
                    requestbody.put("quote_id", String.valueOf(session.getquoetid()));
                    JSONObject mainreqbdy = new JSONObject();
                    mainreqbdy.put("cartItem", requestbody);
                    buynow(mainreqbdy);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void addtocart(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  addtocarturl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("addtocart", ">>" + response);
                Toast.makeText(getApplicationContext(),"item added in cart",Toast.LENGTH_SHORT).show();
                iscart = true;
                int newcartcount = session.getcartcount()+1;
                session.setcartcount(newcartcount);
                textCartItemCount.setText(""+session.getcartcount());
                iconcart.setColorFilter(Color.parseColor("#8dc542"));
                StaticInfo.dialogend();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
                Toast.makeText(getApplicationContext(),"some api issue",Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }

    private void buynow(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        final String requestBody = response.toString();
        Log.d("request",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST,  addtocarturl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("addtocart", ">>" + response);
                iscart = true;
                int newcartcount = session.getcartcount()+1;
                session.setcartcount(newcartcount);
                textCartItemCount.setText(""+session.getcartcount());
                iconcart.setColorFilter(Color.parseColor("#8dc542"));
                Intent intent = new Intent(NewProductActivity.this,CartActivity.class);
                startActivity(intent);
                StaticInfo.dialogend();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Toast.makeText(getApplicationContext(),"some api issue",Toast.LENGTH_SHORT).show();
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }

}