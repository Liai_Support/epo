package com.lia.epo.Fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.epo.Activity.CartActivity;
import com.lia.epo.Activity.MainActivity;
import com.lia.epo.Activity.ProductlistActivity;
import com.lia.epo.Adapter.CartAdapter;
import com.lia.epo.Adapter.HomeSuggestionAdapter;
import com.lia.epo.Adapter.HomeBannerAdapter;
import com.lia.epo.Adapter.HomeCollectionAdapter;
import com.lia.epo.Adapter.HomeCategoryAdapter;
import com.lia.epo.Data.HomeData;
import com.lia.epo.DataModel.AllcategoryDataModel;
import com.lia.epo.DataModel.CartDataModel;
import com.lia.epo.DataModel.HomeDataModel;
import com.lia.epo.DataModel.ProductDataModel;
import com.lia.epo.DataModel.ProductlistDataModel;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView recyclerView1,recyclerView2,recyclerView3,recyclerView4;
    private LinearLayoutManager layoutManager;
    TextView viewall;
    private GridLayoutManager gridLayoutManager;
    private LinearLayoutManager HorizontalLayout;
    public static ArrayList<HomeDataModel> data;
    private Session session;
    Timer timer;
    TimerTask timerTask;
    int position,head=80;
    private static ArrayList<AllcategoryDataModel> data1;
    private static ArrayList<ProductlistDataModel> data2;
    MainActivity activity;

    private String URLstringcartlist="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine";
    private static final String Url = "https://www.vdcprojects.xyz/epo/index.php/rest/english/V1/categories";
    private static final String producturl = "https://www.vdcprojects.xyz/epo/index.php/rest/V1/products";
    private static  final String imgurl = "https://www.vdcprojects.xyz/epo/pub/media/catalog/product";

    public HomeFragment() {
        // Required empty public constructor

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        activity = (MainActivity) getActivity();

        session = new Session(getContext());
        getInitialValues();
        product();
        //getcartdata();
        viewall = view.findViewById(R.id.viewall);
        viewall.setOnClickListener(this);
        recyclerView1 = view.findViewById(R.id.recycler1);
        recyclerView1.setHasFixedSize(true);

        recyclerView1.setLayoutManager(new LinearLayoutManager(view.getContext()));  //this is for vertical
        HorizontalLayout = new LinearLayoutManager(view.getContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView1.setLayoutManager(HorizontalLayout);
        /*data = new ArrayList<HomeDataModel>();
        for (int i = 0; i < HomeData.nameArray.length; i++) {
            data.add(new HomeDataModel(
                    HomeData.nameArray[i],
                    HomeData.versionArray[i],
                    HomeData.id_[i],
                    HomeData.drawableArray[i]
            ));
        }
        recyclerView.setAdapter(new HomeAdapterTop(data));*/

        recyclerView2 = view.findViewById(R.id.recycler2);
        recyclerView2.setHasFixedSize(true);
        recyclerView2.setLayoutManager(new LinearLayoutManager(view.getContext()));  //this is for vertical
        HorizontalLayout = new LinearLayoutManager(view.getContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView2.setLayoutManager(HorizontalLayout);
        data = new ArrayList<HomeDataModel>();
        for (int i = 0; i < HomeData.nameArray.length; i++) {
            data.add(new HomeDataModel(
                    HomeData.nameArray[i],
                    HomeData.versionArray[i],
                    HomeData.id_[i],
                    HomeData.drawableArray[i]
            ));
        }
        recyclerView2.setAdapter(new HomeBannerAdapter(data));
        if(data != null){
            position = Integer.MAX_VALUE / 2;
            recyclerView2.scrollToPosition(position);
        }
        recyclerView2.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == 1) {
                    Log.d("aadd",""+newState);
                    position = HorizontalLayout.findFirstCompletelyVisibleItemPosition();
                    runAutoScrollBanner();
                } else if (newState == 0) {
                    Log.d("aadd0",""+newState);
                    position = HorizontalLayout.findFirstCompletelyVisibleItemPosition();
                    runAutoScrollBanner();
                }
            }
        });

        recyclerView3 = view.findViewById(R.id.recycler3);
        recyclerView3.setHasFixedSize(true);
        recyclerView3.setLayoutManager(new LinearLayoutManager(view.getContext()));  //this is for vertical
        HorizontalLayout = new LinearLayoutManager(view.getContext(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView3.setLayoutManager(HorizontalLayout);
        data = new ArrayList<HomeDataModel>();
        for (int i = 0; i < HomeData.nameArray.length; i++) {
            data.add(new HomeDataModel(
                    HomeData.nameArray[i],
                    HomeData.versionArray[i],
                    HomeData.id_[i],
                    HomeData.drawableArray[i]
            ));
        }
        recyclerView3.setAdapter(new HomeCollectionAdapter(getContext(),data));

        recyclerView4 = view.findViewById(R.id.recycler4);
        recyclerView4.setHasFixedSize(false);
        recyclerView4.setLayoutManager(new LinearLayoutManager(view.getContext()));  //this is for vertical
        HorizontalLayout = new LinearLayoutManager(view.getContext(),LinearLayoutManager.HORIZONTAL,false);
        //gridLayoutManager =new GridLayoutManager(view.getContext(),2,GridLayoutManager.HORIZONTAL, false);
        recyclerView4.setLayoutManager(HorizontalLayout);
        return view;
    }

    private void product() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET,  producturl+"?searchCriteria[page_size]="+head, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("product", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    JSONArray dataa = obj.getJSONArray("items");
                    Log.d("gvbhjn",""+dataa.length());
                    data2 = new ArrayList<>();
                    for(int i=0;i < dataa.length();i++) {
                        JSONObject childobject = dataa.getJSONObject(i);
                        childobject.getString("name");
                        JSONArray img = childobject.getJSONArray("media_gallery_entries");
                        JSONObject imgobj = img.getJSONObject(0);
                        imgobj.getString("file");
                        // Log.d("gbhnj",""+imgurl+imgobj.getString("file"));
                        Log.d("hnjmk,",""+activity.cartsku);
                        String image = imgurl+imgobj.getString("file");
                        ProductlistDataModel productlistDataModel= new ProductlistDataModel(image,childobject.getString("sku"),childobject.getString("name"),childobject.getInt("id"),childobject.getDouble("price"));
                        data2.add(productlistDataModel);
                        recyclerView4.setAdapter(new HomeSuggestionAdapter(getContext(),data2, (MainActivity) getActivity()));
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
                StaticInfo.dialogend();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());

                Log.d("parasfscm",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            public Map<String, String> getParams() throws AuthFailureError{
                Map<String, String> params1 = new HashMap<String, String>();
                params1.put("searchCriteria[page_size]","5");
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                Log.d("cfgvb",""+params1);
                return params1;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                   /* try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }*/
                return null;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }

    private void getInitialValues() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET,  Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("strrrrsdfr", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    JSONArray dataa = obj.getJSONArray("children_data");
                    data1 = new ArrayList<>();
                    for(int i=0;i < dataa.length();i++) {
                        JSONObject childobject = dataa.getJSONObject(i);
                        childobject.getString("name");
                        AllcategoryDataModel allcategoryDataModel= new AllcategoryDataModel(childobject.getString("name"),childobject.getInt("id"),childobject.getInt("product_count"));
                        data1.add(allcategoryDataModel);
                        recyclerView1.setAdapter(new HomeCategoryAdapter(data1,(MainActivity) getActivity()));
                        //  Log.d("teli", "" +childobject);
                        Log.d("teli11", String.valueOf(childobject.getString("name")));
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                   /* try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }*/
                return null;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }

    private void getcartdata() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLstringcartlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("cartresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    // session.setquoteid(obj.getInt("id"));
                    Log.d("fcgvbn",""+session.getquoetid());
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        stopAutoScrollBanner();
    }

    private void stopAutoScrollBanner() {
        if (timer != null && timerTask != null) {
            timerTask.cancel();
            timer.cancel();
            timer = null;
            timerTask = null;
            position = HorizontalLayout.findFirstCompletelyVisibleItemPosition();
        }
    }

    private void runAutoScrollBanner() {
        if (timer == null && timerTask == null) {
            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    if (position == Integer.MAX_VALUE) {
                        position = Integer.MAX_VALUE /Integer.MAX_VALUE;
                        recyclerView2.scrollToPosition(position);
                        recyclerView2.smoothScrollBy(3, 4);
                    } else {
                        position++;
                        recyclerView2.smoothScrollToPosition(position);
                    }
                }
            };
            timer.schedule(timerTask, 4000,4000);
        }
    }

    @Override
    public void onClick(View view) {
        if(view == viewall){
            Intent intent = new Intent(getContext(), ProductlistActivity.class);
            intent.putExtra("categoryname",getString(R.string.allproducts));
            intent.putStringArrayListExtra("cartlist",activity.cartsku);
            getContext().startActivity(intent);
        }
    }
}