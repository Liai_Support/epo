package com.lia.epo.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.lia.epo.Activity.CheckoutActivity;
import com.lia.epo.Activity.MainActivity;
import com.lia.epo.Activity.NewCartActivity;
import com.lia.epo.Activity.NewCheckoutActivity;
import com.lia.epo.CountryData;
import com.lia.epo.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CheckoutDeliveryinfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CheckoutDeliveryinfoFragment extends Fragment implements View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public String address="";
    Button next;
    CheckBox saveaddressinfo;
    CheckoutActivity activity;
    AutoCompleteTextView country;
    public EditText name,pnumber,addr,city,state,pincode,landmark;
    public String dname,dphno,daddr,dcity,dstate,dpincode,dlandmark,dcountry;
    public JSONArray daddraaray = new JSONArray();


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String URLstringplaceorder1="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine/estimate-shipping-methods";
    private String URLstringplaceorder2="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine/shipping-information";

    public String selectedCountryCode;
    public CheckoutDeliveryinfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Checkout2Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CheckoutDeliveryinfoFragment newInstance(String param1, String param2) {
        CheckoutDeliveryinfoFragment fragment = new CheckoutDeliveryinfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_checkout_deliveryinfo, container, false);
        activity = (CheckoutActivity) getActivity();
        saveaddressinfo = view.findViewById(R.id.chekbox_delivery_save);
        name = view.findViewById(R.id.edit_delivery_name);
        pnumber = view.findViewById(R.id.edit_delivery_ph_no);
        addr = view.findViewById(R.id.edit_delivery_doorno);
        city = view.findViewById(R.id.edit_delivery_city);
        state = view.findViewById(R.id.edit_delivery_state);
        pincode = view.findViewById(R.id.edit_delivery_pincode);
        landmark = view.findViewById(R.id.edit_delivery_landmark);
        next = view.findViewById(R.id.next);
        next.setOnClickListener(this);

        name.setText(""+activity.session.getdname());
        pnumber.setText(""+activity.session.getdphno());
        addr.setText(""+activity.session.getdaddr());
        city.setText(""+activity.session.getdcity());
        state.setText(""+activity.session.getdstate());
        pincode.setText(""+activity.session.getdpincode());
        landmark.setText(""+activity.session.getdlandmark());
      //  country.setText(""+activity.session.getdcountry());


        saveaddressinfo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // update your model (or other business logic) based on isChecked
                if(saveaddressinfo.isChecked() == true){
                    activity.session.setdname(""+name.getText().toString());
                    activity.session.setdphno(""+pnumber.getText().toString());
                    activity.session.setdaddr(""+addr.getText().toString());
                    activity.session.setdcity(""+city.getText().toString());
                    activity.session.setdstate(""+state.getText().toString());
                    activity.session.setdpincode(""+pincode.getText().toString());
                    activity.session.setdlandmark(""+landmark.getText().toString());
//                    activity.session.setdcountry(""+country.getText().toString());
                    Log.d("dshgvfd","select");
                }
                else if(saveaddressinfo.isChecked() == false){
                    Log.d("dshgvfd","unselect");
                    activity.session.setdname("");
                    activity.session.setdphno("");
                    activity.session.setdaddr("");
                    activity.session.setdcity("");
                    activity.session.setdstate("");
                    activity.session.setdpincode("");
                    activity.session.setdlandmark("");
                    activity.session.setdcountry("");
                }
            }
        });        /*next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment = new CheckoutPaymentFragment();
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout,fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });*/
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), R.layout.dropdown_menu_popup_item, CountryData.countryNames);
        AutoCompleteTextView country = (AutoCompleteTextView) view.findViewById(R.id.filled_exposed_dropdown);
        country.setText(""+activity.session.getdcountry());
        selectedCountryCode="US";
        country.setThreshold(1);
        country.setAdapter(adapter);
        country.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int index = Arrays.asList(CountryData.countryNames).lastIndexOf(""+s);
                String code = CountryData.countryAreaCodes[index];
                Log.d("sd112",""+code);
                selectedCountryCode=code;
                Log.d("sd",""+selectedCountryCode);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });
        Log.d("sd",""+selectedCountryCode);
        return view;
    }

    @Override
    public void onClick(View view) {
        if(view == next){

           /* Fragment fragment = new CheckoutPaymentFragment();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.frameLayout,fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
*/
            dname= name.getText().toString();
            dphno= pnumber.getText().toString();
            daddr= addr.getText().toString();
           // daddraaray = new JSONArray();
            daddraaray.put(daddr);
            dcity= city.getText().toString();
            dstate= state.getText().toString();
            dpincode= pincode.getText().toString();
            dlandmark= landmark.getText().toString();
         address=String.valueOf(dname+","+addr.getText().toString()+","+dcity+"-"+dpincode+","+dstate+".");


            JSONObject requestbody1 = new JSONObject();
            try {
                requestbody1.put("region", dstate);
                requestbody1.put("region_id", 12);
                requestbody1.put("region_code", "CA");
                requestbody1.put("country_id", selectedCountryCode);
                requestbody1.put("street", daddraaray);
                requestbody1.put("postcode", dpincode);
                requestbody1.put("city", dcity);
                requestbody1.put("firstname", dname);
                requestbody1.put("lastname", dname);
                requestbody1.put("customer_id", activity.session.getUserId());
                requestbody1.put("email", "");
                requestbody1.put("telephone", dphno);
                requestbody1.put("same_as_billing", 1);
                JSONObject mainreqbdy1 = new JSONObject();
                mainreqbdy1.put("address", requestbody1);
                placeorderrequestJSON1(mainreqbdy1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    private void placeorderrequestJSON1(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("request1",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLstringplaceorder1, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("placeorderresponse1", ">>" + response);
                try{
                    JSONArray arr = new JSONArray(response);
                    JSONObject obj = arr.getJSONObject(0);
                    obj.getString("carrier_code");
                    Log.d("scvd",""+obj.getString("carrier_code"));

                    JSONObject shipping = new JSONObject();
                    shipping.put("region", dstate);
                    shipping.put("region_id", 12);
                    shipping.put("region_code", "CA");
                    shipping.put("country_id", selectedCountryCode);
                    shipping.put("street", daddraaray);
                    shipping.put("postcode", dpincode);
                    shipping.put("city", dcity);
                    shipping.put("firstname", dname);
                    shipping.put("lastname", dname);
                   // shipping.put("customer_id", activity.session.getUserId());
                    shipping.put("email", "");
                    shipping.put("telephone", dphno);
                   // shipping.put("same_as_billing", 1);
                    JSONObject mainshiping = new JSONObject();
                    mainshiping.put("shipping_address", shipping);

                    JSONObject billing = new JSONObject();
                    billing.put("region", dstate);
                    billing.put("region_id", 12);
                    billing.put("region_code", "CA");
                    billing.put("country_id", selectedCountryCode);
                    billing.put("street", daddraaray);
                    billing.put("postcode", dpincode);
                    billing.put("city", dcity);
                    billing.put("firstname", dname);
                    billing.put("lastname", dname);
                   // billing.put("customer_id", activity.session.getUserId());
                    billing.put("email", "");
                    billing.put("telephone", dphno);
                  //  billing.put("same_as_billing", 1);
                    JSONObject mainbilling = new JSONObject();
                    mainbilling.put("billing_address", billing);
                    activity.billingdata = billing;


                    JSONObject mergebdy = new JSONObject();
                    mergebdy.put("shipping_address",shipping);
                    mergebdy.put("billing_address", billing);
                    mergebdy.put("shipping_carrier_code",obj.getString("carrier_code"));
                    mergebdy.put("shipping_method_code",obj.getString("method_code"));

                    JSONObject mainreqbdy2 = new JSONObject();
                    mainreqbdy2.put("addressInformation",mergebdy);
                    placeorderrequestJSON2(mainreqbdy2);

                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +activity.session.getToken());
                Log.d("param",""+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void placeorderrequestJSON2(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("request1",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLstringplaceorder2, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("placeorderresponse2", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    JSONArray arr = obj.getJSONArray("payment_methods");
                    JSONObject payment = arr.getJSONObject(0);
                    String paymentcode = payment.getString("code");
                    activity.paymentcode = payment.getString("code");

                    JSONObject total = obj.getJSONObject("totals");
                    String shipping_amount = total.getString("shipping_amount");
                    activity.shipping_amount = total.getString("shipping_amount");

//                    Fragment fragment = new CheckoutOrdersummuryFragment();
//                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                    fragmentTransaction.replace(R.id.frameLayout,fragment);
//                    fragmentTransaction.addToBackStack(null);
//                    fragmentTransaction.commit();
                    Intent myIntent = new Intent(getActivity(), NewCheckoutActivity.class);
                    myIntent.putExtra("addressuser", address);

                    getActivity().startActivity(myIntent);

                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +activity.session.getToken());
                Log.d("param",""+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }


}