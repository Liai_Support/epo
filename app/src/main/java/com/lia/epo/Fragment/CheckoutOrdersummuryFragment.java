package com.lia.epo.Fragment;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.epo.Activity.CartActivity;
import com.lia.epo.Activity.CheckoutActivity;
import com.lia.epo.Activity.MainActivity;
import com.lia.epo.Adapter.CartAdapter;
import com.lia.epo.Adapter.OrderSummaryAdapter;
import com.lia.epo.Data.OrderSummaryData;
import com.lia.epo.DataModel.CartDataModel;
import com.lia.epo.DataModel.OrderSummaryDataModel;
import com.lia.epo.R;
import com.lia.epo.Session;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CheckoutOrdersummuryFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CheckoutOrdersummuryFragment extends Fragment implements View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView recyclerView;
    TextView txt_itemcount,txt_subtotal,txt_shippingcost,txt_total;
    Button next;
    private LinearLayoutManager layoutManager;
    private static ArrayList<CartDataModel> data;
    private String URLstringcartlist="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine";
    private String URLstringplaceorder3="https://www.vdcprojects.xyz/epo/index.php/rest/V1/carts/mine/payment-information";
    private static final String url2 = "https://www.vdcprojects.xyz/epo/index.php/rest/english/V1/products?searchCriteria[filter_groups][0][filters][0][field]=sku&searchCriteria[filter_groups][0][filters][0][value]=";
    private static  final String imgurl = "https://www.vdcprojects.xyz/epo/pub/media/catalog/product";
    private Session session;
    CheckoutActivity activity;
    JSONObject billingaddr;

    public CheckoutOrdersummuryFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Checkout1Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CheckoutOrdersummuryFragment newInstance(String param1, String param2) {
        CheckoutOrdersummuryFragment fragment = new CheckoutOrdersummuryFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        session = new Session(getContext());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_checkout_ordersummury, container, false);
        activity = (CheckoutActivity) getActivity();
        next = view.findViewById(R.id.next);
        txt_itemcount = view.findViewById(R.id.order_total_items);
        txt_itemcount.setText(""+activity.itemcount);
        txt_subtotal = view.findViewById(R.id.subtotal);
        txt_shippingcost = view.findViewById(R.id.shipping_cost);
        txt_total = view.findViewById(R.id.total);
        txt_subtotal.setText(""+activity.subtotal+" SAR");
        txt_shippingcost.setText(""+activity.shipping_amount+" SAR");
        Double total = activity.subtotal+Double.valueOf(activity.shipping_amount);
        txt_total.setText(""+total+" SAR");
        recyclerView = view.findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        data = new ArrayList<CartDataModel>();
        getcartdata();
        next.setOnClickListener(this);

        Log.d("sfcavgzb","-"+activity.shipping_amount+"-"+activity.paymentcode+"-"+activity.shipping_amount);
        return view;
    }

    private void getcartdata() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLstringcartlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("cartresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    obj.getInt("items_count");
                    obj.getInt("items_qty");
                  //  session.setcartcount(obj.getInt("items_qty"));
                   // textCartItemCount.setText(""+session.getcartcount());
                    JSONArray dataa = new JSONArray(obj.getString("items"));
                    data = new ArrayList<>();
                  //  itemcount = dataa.length();
                    for(int i=0;i < dataa.length();i++) {
                        JSONObject childobject = dataa.getJSONObject(i);
                        childobject.getString("name");
                        childobject.getDouble("price");
                        childobject.getDouble("qty");
                        url2function(childobject.getString("sku"),childobject.getString("name"),childobject.getDouble("price"),childobject.getInt("item_id"),childobject.getInt("qty"));
                       /* CartDataModel cartDataModel= new CartDataModel(childobject.getString("sku"),childobject.getString("name"),childobject.getDouble("price"),childobject.getInt("item_id"),childobject.getInt("qty"));
                        data.add(cartDataModel);*/
                        //setupAllcategoryData(data);
                        //  Log.d("teli", "" +childobject);
                        Log.d("teli11", String.valueOf(childobject.getString("name")));
                        Log.d("teli1", String.valueOf(childobject.getInt("item_id")));
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }

    private void url2function(final String sku, final String name, final Double price, final int item_id, final int qty) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET,  url2+sku, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("url2response", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    JSONArray dataa = obj.getJSONArray("items");
                    Log.d("gvbhjnvghbj",""+dataa.length());
                    // data = new ArrayList<>();
                    JSONObject childobject = dataa.getJSONObject(0);
                    childobject.getString("name");
                    JSONArray img = childobject.getJSONArray("media_gallery_entries");
                    JSONObject imgobj = img.getJSONObject(0);
                    imgobj.getString("file");
                    Log.d("imgurl",""+imgurl+imgobj.getString("file"));
                    String image = imgurl+imgobj.getString("file");

                    CartDataModel cartDataModel= new CartDataModel(image,sku,name,price,item_id,qty);
                    data.add(cartDataModel);
                    recyclerView.setAdapter(new OrderSummaryAdapter(data));

                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                   /* try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }*/
                return null;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }



    @Override
    public void onClick(View view) {
        if(view == next){
            JSONObject obj = new JSONObject();
            try {
                obj.put("method",activity.paymentcode);
                JSONObject paymentmethod = new JSONObject();
                paymentmethod.put("paymentMethod", obj);

                JSONObject mainbody = new JSONObject();
                mainbody.put("paymentMethod",obj);
                mainbody.put("billing_address",activity.billingdata);
                placeorderrequestJSON3(mainbody);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void placeorderrequestJSON3(JSONObject response) {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        final String requestBody = response.toString();
        Log.d("request1",String.valueOf(response));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLstringplaceorder3, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("placeorderresponse3", ">>" + response);
                    String object = response;
                    Fragment fragment = new CheckoutOrderplacedFragment();
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frameLayout,fragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +activity.session.getToken());
                Log.d("param",""+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return requestBody == null ? null : requestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                    return null;
                }
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }

}