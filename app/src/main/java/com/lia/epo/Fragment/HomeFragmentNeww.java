package com.lia.epo.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.lia.epo.Activity.MainActivity;
import com.lia.epo.Activity.ProductlistActivity;
import com.lia.epo.Adapter.NewHomeBannerAdapter;
import com.lia.epo.Adapter.NewHomeCategoryAdapter;
import com.lia.epo.Adapter.NewHomeCollectionAdapter;
import com.lia.epo.Adapter.NewHomeSuggestionAdapter;
import com.lia.epo.Data.HomeData;
import com.lia.epo.DataModel.AllcategoryDataModel;
import com.lia.epo.DataModel.HomeDataModel;
import com.lia.epo.DataModel.ProductlistDataModel;
import com.lia.epo.R;
import com.lia.epo.Session;
import com.lia.epo.StaticInfo;
import com.lia.epo.Urls.urlList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static com.lia.epo.Urls.urlList.Url;
import static com.lia.epo.Urls.urlList.imgurl;
import static com.lia.epo.Urls.urlList.producturl;

public class HomeFragmentNeww extends Fragment implements View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView secondRowCategory,firstRowMainBanner,specialForYouRV,popularProductRV;
    private LinearLayoutManager layoutManager;
    TextView viewall,textcartcount;
    private GridLayoutManager gridLayoutManager;
    private LinearLayoutManager HorizontalLayout;
    public static ArrayList<HomeDataModel> data;
    private Session session;
    Timer timer;
    TimerTask timerTask;
    int position,head=80;
    private static ArrayList<AllcategoryDataModel> data1;
    private static ArrayList<ProductlistDataModel> data2;
    MainActivity activity;

    public HomeFragmentNeww() {
        // Required empty public constructor

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewHomeFragment newInstance(String param1, String param2) {
        NewHomeFragment fragment = new NewHomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_new, container, false);
        activity = (MainActivity) getActivity();

//------------------------------------------------------------------------------------------------------------------------
        session = new Session(getContext());

        getInitialValues();
        product();
        //getcartdata();
        viewall = view.findViewById(R.id.viewall);
        viewall.setOnClickListener(this);

        secondRowCategory = view.findViewById(R.id.homeSecondRowBanner);
        secondRowCategory.setHasFixedSize(true);
        secondRowCategory.setLayoutManager(new LinearLayoutManager(view.getContext()));  //this is for vertical
        HorizontalLayout = new LinearLayoutManager(view.getContext(),LinearLayoutManager.HORIZONTAL,false);
        secondRowCategory.setLayoutManager(HorizontalLayout);

        /*data = new ArrayList<HomeDataModel>();
        for (int i = 0; i < HomeData.nameArray.length; i++) {
            data.add(new HomeDataModel(
                    HomeData.nameArray[i],
                    HomeData.versionArray[i],
                    HomeData.id_[i],
                    HomeData.drawableArray[i]
            ));
        }
        recyclerView.setAdapter(new HomeAdapterTop(data));*/

        firstRowMainBanner = view.findViewById(R.id.homeMainBanner);
        firstRowMainBanner.setHasFixedSize(true);
        firstRowMainBanner.setLayoutManager(new LinearLayoutManager(view.getContext()));  //this is for vertical
        HorizontalLayout = new LinearLayoutManager(view.getContext(),LinearLayoutManager.HORIZONTAL,false);
        firstRowMainBanner.setLayoutManager(HorizontalLayout);
        data = new ArrayList<HomeDataModel>();
        for (int i = 0; i < HomeData.nameArray.length; i++) {
            data.add(new HomeDataModel(
                    HomeData.nameArray[i],
                    HomeData.versionArray[i],
                    HomeData.id_[i],
                    HomeData.drawableArray[i]
            ));
        }
        firstRowMainBanner.setAdapter(new NewHomeBannerAdapter(data));
        if(data != null){
            position = Integer.MAX_VALUE / 2;
            firstRowMainBanner.scrollToPosition(position);
        }
        firstRowMainBanner.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == 1) {
                    Log.d("aadd",""+newState);
                    position = HorizontalLayout.findFirstCompletelyVisibleItemPosition();
                    runAutoScrollBanner();
                } else if (newState == 0) {
                    Log.d("aadd0",""+newState);
                    position = HorizontalLayout.findFirstCompletelyVisibleItemPosition();
                    runAutoScrollBanner();
                }
            }
        });

        specialForYouRV = view.findViewById(R.id.specialRecyclerView);
        specialForYouRV.setHasFixedSize(true);
        specialForYouRV.setLayoutManager(new LinearLayoutManager(view.getContext()));  //this is for vertical
        HorizontalLayout = new LinearLayoutManager(view.getContext(),LinearLayoutManager.HORIZONTAL,false);
        specialForYouRV.setLayoutManager(HorizontalLayout);
        data = new ArrayList<HomeDataModel>();
        for (int i = 0; i < HomeData.nameArray.length; i++) {
            data.add(new HomeDataModel(
                    HomeData.nameArray[i],
                    HomeData.versionArray[i],
                    HomeData.id_[i],
                    HomeData.drawableArray[i]
            ));
        }
        specialForYouRV.setAdapter(new NewHomeCollectionAdapter(getContext(),data));

        popularProductRV = view.findViewById(R.id.popularProductRV);
        popularProductRV.setHasFixedSize(false);
        popularProductRV.setLayoutManager(new LinearLayoutManager(view.getContext()));  //this is for vertical
        HorizontalLayout = new LinearLayoutManager(view.getContext(),LinearLayoutManager.HORIZONTAL,false);
        //gridLayoutManager =new GridLayoutManager(view.getContext(),2,GridLayoutManager.HORIZONTAL, false);
        popularProductRV.setLayoutManager(HorizontalLayout);
        return view;
    }

    private void product() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET,  producturl+"?searchCriteria[page_size]="+head, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("product", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    JSONArray dataa = obj.getJSONArray("items");
                    Log.d("gvbhjn",""+dataa.length());
                    data2 = new ArrayList<>();
                    for(int i=0;i < dataa.length();i++) {
                        JSONObject childobject = dataa.getJSONObject(i);
                        childobject.getString("name");
                        JSONArray img = childobject.getJSONArray("media_gallery_entries");
                        JSONObject imgobj = img.getJSONObject(0);
                        imgobj.getString("file");
                        // Log.d("gbhnj",""+imgurl+imgobj.getString("file"));
                        Log.d("hnjmk,",""+activity.cartsku);
                        String image = imgurl+imgobj.getString("file");
                        ProductlistDataModel productlistDataModel= new ProductlistDataModel(image,childobject.getString("sku"),childobject.getString("name"),childobject.getInt("id"),childobject.getDouble("price"));
                        data2.add(productlistDataModel);
                        popularProductRV.setAdapter(new NewHomeSuggestionAdapter(getContext(),data2, (MainActivity) getActivity()));
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
                StaticInfo.dialogend();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                StaticInfo.dialogend();
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());

                Log.d("parasfscm",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            public Map<String, String> getParams() throws AuthFailureError{
                Map<String, String> params1 = new HashMap<String, String>();
                params1.put("searchCriteria[page_size]","5");
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                Log.d("cfgvb",""+params1);
                return params1;
            }
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                   /* try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }*/
                return null;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }
//SECOND ROW CATEGORY DATA
    private void getInitialValues() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET,  Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("strrrrsdfr", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    JSONArray dataa = obj.getJSONArray("children_data");
                    data1 = new ArrayList<>();
                    for(int i=0;i < dataa.length();i++) {
                        JSONObject childobject = dataa.getJSONObject(i);
                        childobject.getString("name");
                        AllcategoryDataModel allcategoryDataModel= new AllcategoryDataModel(childobject.getString("name"),childobject.getInt("id"),childobject.getInt("product_count"));
                        data1.add(allcategoryDataModel);
                        secondRowCategory.setAdapter(new NewHomeCategoryAdapter(data1,(MainActivity) getActivity()));
                        //  Log.d("teli", "" +childobject);
                        Log.d("teli11", String.valueOf(childobject.getString("name")));
                    }
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }) {
            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                   /* try {
                        return requestcartBody == null ? null : requestcartBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestcartBody, "utf-8");
                        return null;
                    }*/
                return null;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(stringRequest);
    }

    private void getcartdata() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlList.URLstringcartlist, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("cartresponse", ">>" + response);
                try{
                    JSONObject obj = new JSONObject(response);
                    // session.setquoteid(obj.getInt("id"));
                    Log.d("fcgvbn",""+session.getquoetid());
                }
                catch (JSONException e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("VOLLEY", error.toString());
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                int id = session.getUserId();
                String tok = session.getToken();
                Map<String, String> params = new HashMap<String, String>();
                //params.put("Content-Type","application/json" );
                params.put("Authorization", "Bearer " +session.getToken());
                Log.d("param",""+params);
                Log.d("id",""+id);
                Log.d("token",""+tok);
                return params;
            }
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("User", UserName);
//                params.put("Pass", PassWord);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/json; charset=utf-8";
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Log.e("VOLLEY1",""+stringRequest);
        requestQueue.add(stringRequest);
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        stopAutoScrollBanner();
    }

    private void stopAutoScrollBanner() {
        if (timer != null && timerTask != null) {
            timerTask.cancel();
            timer.cancel();
            timer = null;
            timerTask = null;
            position = HorizontalLayout.findFirstCompletelyVisibleItemPosition();
        }
    }

    private void runAutoScrollBanner() {
        if (timer == null && timerTask == null) {
            timer = new Timer();
            timerTask = new TimerTask() {
                @Override
                public void run() {
                    if (position == Integer.MAX_VALUE) {
                        position = Integer.MAX_VALUE /Integer.MAX_VALUE;
                        firstRowMainBanner.scrollToPosition(position);
                        firstRowMainBanner.smoothScrollBy(3, 4);
                    } else {
                        position++;
                        firstRowMainBanner.smoothScrollToPosition(position);
                    }
                }
            };
            timer.schedule(timerTask, 4000,4000);
        }
    }

    @Override
    public void onClick(View view) {
        if(view == viewall){
            Intent intent = new Intent(getContext(), ProductlistActivity.class);
            intent.putExtra("categoryname",getString(R.string.allproducts));
            intent.putStringArrayListExtra("cartlist",activity.cartsku);
            getContext().startActivity(intent);
        }
    }

}
