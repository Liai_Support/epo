package com.lia.epo;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;

public class Session {

    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    Context _context;



    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences constants
    private static final String PREF_NAME = "MyPreference";
    private static final String IS_NETWORK = "IsNetwork";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String REMEMBER_ME = "IsRrememberMe";
    private static final String IS_LOGIN = "IsLoggedIn";


    public Session(Context context) {
        this._context = context;
        prefs = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = prefs.edit();
    }

//    -----------------------------------------------------------------------------------------
    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return prefs.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
//-------------------------------------------------------------------------------------------------

    public void isRememberMe(boolean isRememberMe) {
        editor.putBoolean(REMEMBER_ME, isRememberMe);
        editor.commit();
    }

    public boolean RememberMe() {
        return prefs.getBoolean(REMEMBER_ME, true);
    }
public void setusername(String username) {
    prefs.edit().putString("username", username).commit();
}

    public String getusername() {
        String username = prefs.getString("username","");
        return username;
    }
//    -------

    public void setpassword(String password) {
        prefs.edit().putString("password", password).commit();
    }

    public String getpassword() {
        String password = prefs.getString("password","");
        return password;
    }
//-----------
    public void setfirstname(String firstname) {
        prefs.edit().putString("firstname", firstname).commit();
    }


    public String getfirstname() {
        String firstname = prefs.getString("firstname","");
        return firstname;
    }

    public void setlastname(String lastname) {
        prefs.edit().putString("lastname", lastname).commit();
    }


    public String getlastname() {
        String lastname = prefs.getString("lastname","");
        return lastname;
    }
    public void setdname(String dname) {
        prefs.edit().putString("dname", dname).commit();
    }

    public String getdname() {
        String dname = prefs.getString("dname","");
        return dname;
    }

    public void setdphno(String dphno) {
        prefs.edit().putString("dphno", dphno).commit();
    }

    public String getdphno() {
        String dphno = prefs.getString("dphno","");
        return dphno;
    }

    public void setdaddr(String daddr) {
        prefs.edit().putString("daddr", daddr).commit();
    }

    public String getdaddr() {
        String daddr = prefs.getString("daddr","");
        return daddr;
    }

    public void setdcity(String dcity) {
        prefs.edit().putString("dcity", dcity).commit();
    }

    public String getdcity() {
        String dcity = prefs.getString("dcity","");
        return dcity;
    }

    public void setdstate(String dstate) {
        prefs.edit().putString("dstate", dstate).commit();
    }

    public String getdstate() {
        String dstate = prefs.getString("dstate","");
        return dstate;
    }

    public void setdpincode(String dpincode) {
        prefs.edit().putString("dpincode", dpincode).commit();
    }

    public String getdpincode() {
        String dpincode = prefs.getString("dpincode","");
        return dpincode;
    }

    public void setdlandmark(String dlandmark) {
        prefs.edit().putString("dlandmark", dlandmark).commit();
    }

    public String getdlandmark() {
        String dlandmark = prefs.getString("dlandmark","");
        return dlandmark;
    }

    public void setdcountry(String dcountry) {
        prefs.edit().putString("dcountry", dcountry).commit();
    }

    public String getdcountry() {
        String dcountry = prefs.getString("dcountry","");
        return dcountry;
    }
    public void setToken(String token) {

        prefs.edit().putString("token", token).commit();
    }

    public String getToken() {
        String token = prefs.getString("token","");
        return token;
    }

    public void setsplash(int splash) {
        prefs.edit().putInt("splash", splash).commit();
    }

    public int getsplash() {
        int splash = prefs.getInt("splash",0);
        return splash;
    }

//    language page
public void setLangPage(int LangPage) {
    prefs.edit().putInt("LangPage", LangPage).commit();
}

    public int getLangPage() {
        int LangPage = prefs.getInt("LagnPage",0);
        return LangPage;
    }




    public void setUserId(int userId) {
        prefs.edit().putInt("userId", userId).commit();
    }
    public int getUserId() {
        int userId = prefs.getInt("userId",0);
        return userId;
    }

    public void setwebsiteid(int websiteid) {
        prefs.edit().putInt("websiteid", websiteid).commit();
    }
    public int getwebsiteid() {
        int websiteid = prefs.getInt("websiteid",0);
        return websiteid;
    }

    public void setcartcount(int cartcount) {
        prefs.edit().putInt("cartcount", cartcount).commit();
    }
    public int getcartcount() {
        int cartcount = prefs.getInt("cartcount",0);
        return cartcount;
    }

    public void setquoteid(int quoetid) {
        prefs.edit().putInt("quoetid", quoetid).commit();
    }
    public int getquoetid() {
        int quoetid = prefs.getInt("quoetid",0);
        return quoetid;
    }

    public String getmailid() {
        String mailid = prefs.getString("mailid","");
        return mailid;
    }
    public void setmailid(String mailid) {
        prefs.edit().putString("mailid", mailid).commit();
    }



    public void setlang(String Lang) {
        prefs.edit().putString("Lang",Lang).commit();
    }

    public String getlang() {
        String Lang = prefs.getString("Lang","en");
        return Lang;
    }

    public void setIsNetwork(boolean isNetwork){
        editor.putBoolean(IS_NETWORK, isNetwork);
        editor.commit();
    }

    public boolean isNetwork(){
        boolean connected = false;
        try {
            ConnectivityManager cm = (ConnectivityManager) _context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo nInfo = cm.getActiveNetworkInfo();
            connected = nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
            return prefs.getBoolean(IS_NETWORK,connected);
        }
        catch (Exception e) {
            Log.e("Connectivity Exception", e.getMessage());
        }
        return prefs.getBoolean(IS_NETWORK,connected);
    }

    public void nonetwork(){
        AlertDialog.Builder alertDialog3 = new AlertDialog.Builder(_context);
        // LayoutInflater inflater = getLayoutInflater();
        LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialoglayout = inflater.inflate(R.layout.no_network, null);
        dialoglayout.setMinimumHeight(100);
        dialoglayout.setMinimumWidth(100);
        dialoglayout.setBackgroundDrawable(null);
        alertDialog3.setView(R.layout.no_network);
        final AlertDialog alertDialog = alertDialog3.create();
        alertDialog.show();
        alertDialog.setCancelable(true);
        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // dialog dismiss without button press
               /* if (isNetwork() == true){
                    alertDialog.dismiss();
                }
                else {
                    alertDialog.show();
                }*/
            }
        });
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = 500;
        layoutParams.height = 500;
        alertDialog.getWindow().setBackgroundDrawable(null);
        alertDialog.getWindow().setAttributes(layoutParams);
        ImageView btnClose = (ImageView)alertDialog.findViewById(R.id.btnIvClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (isNetwork() == true){
                    alertDialog.dismiss();
                }
                else {
                    alertDialog.show();
                }
            }
        });
    }
}
