package com.lia.epo.Data;

import com.lia.epo.R;

public class WishlistData {
    public static String[] nameArray = {"Cute bunny Doll", "Kitchen Product","Kitchen", "Bags & Pouches","Interiors","Bags & Pouches","Interiors"};
    public static String[] versionArray = {"53.00 $", "53.00$","57.00$","55.00$","54.00$","55.00$","54.00$"};
    public static Integer[] drawableArray = {R.drawable.mask_group_93, R.drawable.group_335,R.drawable.mask_group_55, R.drawable.group_317, R.drawable.group_318,R.drawable.group_317, R.drawable.group_318};
    public static Integer[] id_ = {0, 1,2,3,4,5,6};
}
