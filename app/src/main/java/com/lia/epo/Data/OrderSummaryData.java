package com.lia.epo.Data;

import com.lia.epo.R;

public class OrderSummaryData {
    public static String[] nameArray = {"Cute bunny Doll", "Kitchen Product"};
    public static String[] versionArray = {"53.00 $", "53.00$"};
    public static Integer[] drawableArray = {R.drawable.mask_group_93, R.drawable.group_335};
    public static Integer[] id_ = {0, 1};
}
