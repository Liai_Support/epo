package com.lia.epo.Data;

import com.lia.epo.R;

public class HomeData {
    public static String[] nameArray = {"Cute bunny Doll", "Kitchen Product","Kitchen", "Bags & Pouches","Interiors","Bags & Pouches","Interiors"};
    public static String[] versionArray = {"53.00 $", "53.00$","57.00$","55.00$","54.00$","55.00$","54.00$"};
    public static Integer[] drawableArray = {R.drawable.splash_image2, R.drawable.splash_image3,R.drawable.splash_image2, R.drawable.splash_image2, R.drawable.splash_image1,R.drawable.splash_image3, R.drawable.splash_image1};
    public static Integer[] id_ = {0, 1,2,3,4,5,6};
}
